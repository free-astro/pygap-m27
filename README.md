# README (EN)

## pyGAP-M27

**pyGAP-M27** :  **Py**thon-**G**imp **A**stronomy **P**lugin by **M27**trognondepomme

these are a set of Gimp plugins for astronomy written in python-fu.
It facilitates and automates certain processing carried out on deep sky images.

## Installation

1. Copy the archive or clone the repository on the computer.
( note: the pygap-m27 folder must be writable by gimp)
2. Start Gimp
3. Click on the menu "Edit> Preferences"
4. Open the "Folders> Plugins" dialog box and add the following the path:
    * ..\<path\>..\pygap-m27\pythonfu
5. Open the "Folders> Scripts" dialog box and add the following the path
	* ..\<path\>..\pygap-m27\scriptfu
6. Validate and Restart Gimp
7. a new menu 'PyGapM27' appears in "Filters" menu
8. Click on "Filters>PyGapM27>Menu Initialisation"
8. Restart Gimp
8. a new menu 'PyGapM27' appears next to "Filters"

note: 

* on ubuntu 20.04 , it should set the default python :
`sudo apt-get install python-is-python3`

# README (FR)

## pyGAP-M27

**pyGAP-M27** :  **Py**thon-**G**imp **A**stronomy **P**lugin by **M27**trognondepomme

ce sont un ensemble de plugins Gimp d'astronomie écrit en python-fu.
Il facilite et automatise certains traitements effectués sur les images de ciel profond.


## Installation

1. Copie l'archive puis la dézipper ou cloner le dépôt sur l'ordinateur.
( note: il faut que le dossier pygap-m27 soit accessible en écriture par gimp)
2. Lancer Gimp
3. Appeler le menu "Edition>Préferences"
4. Ouvrir la boite de dialogue "Dossiers> Greffons" et ajouter le chemin suivant :
	* ..\<path\>..\pygap-m27\pythonfu
5. Ouvrir la boite de dialogue "Dossiers> Scripts" et ajouter le chemin suivant :
	* ..\<path\>..\pygap-m27\scriptfu
6. Valider et Relancer Gimp
7. un nouveau menu 'PyGapM27' apparait dans le menu "Filtres"
8. Cliquer sur "Filtres>PyGapM27>Menu Initialisation"
6. Relancer Gimp
7. un nouveau menu 'PyGapM27' apparait à côté de "Filtres"

note: 
* sur ubuntu 20.04 , il faut initialiser le python par défaut:
`sudo apt-get install python-is-python3`
