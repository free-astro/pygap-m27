#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,menu_noise,init_language
from pyGapM27 import info_right0 as info_right
init_language()

import gp2_func  as gimp
from libTools   import check_compatibilty
from libTools   import copy_layer,create_layer,fill_layer
from plug_in    import colors_channel_mixer
from gp2_plugin import CPlugin

class CColorNoiseReduction( CPlugin):
    def __init__(self):
        self.MenuName=_("Color Noise Reduction")
        shelp = ( _("Color Noise Reduction"),
                  _("source")
                  +": https://www.pixinsight.com/doc/legacy/LE/21_noise_reduction/scnr/scnr.html" )
        params = [
               self.Param("INT32", 'color' , _("Color to remove"), 0),
               self.Param("INT32", 'method', _("Method")         , 0),
               self.Param("FLOAT", 'amount', _("Amount")         , 0.0 )]
        gui = {
            'color'   : self.Box("SF-OPTION"    , '(list "' +_('Green') +'" "' +_('Red') +'" "' +_('Blue') +'" )' ) ,
            'method'  : self.Box("SF-OPTION"    , '(list "' +_('Average Neutral') +'" "' +_('Maximum Mask') +'" "' +_('Additive Mask') +'" "' +_('Maximum Neutral') +'" )') ,
            'amount'  : self.Box("SF-ADJUSTMENT", '(list 0.1 0.0 1.0  0.01 0.1 3 0 )' ) }

        CPlugin.__init__( self,
            Pkg+"ColorNoiseReduction"  , # name
            _("Color Noise Reduction") , # blurb
            shelp                      , # help
            info_right                 , # author,copyright, ...
            self.MenuName              , # menu_label
            menu_noise                 , # menu path
            "RGB*"                     , # image_types
            params                     , # params
            gui, dir_scriptfu          ) # gui

    def Processing(self, timg, tdrawable):
        (self.color, method, self.amount) = self.get_parameters_value()

        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr

        gimp.image_undo_group_start(timg)

        self.timg      = timg
        self.tdrawable = tdrawable
        
        if method == 0 :
            self.AverageNeutralProtection()
        if method == 1 :
            self.MaximumMaskProtection()
        if method == 2 :
            self.AdditiveMaskProtection()
        if method == 3 :
            self.MaximumNeutralProtection()

        gimp.image_undo_group_end(timg)
        gimp.displays_flush()

        return ( "SUCCESS","")

    # --------------------------------------------------------------------------    
    def MaximumMaskProtection(self):
        ''' 
        example with Green :
            m = Max( R, B )
            G' = G×(1 — a)×(1 — m) + m×G
        '''
        self.Max2Composantes( self.timg,self.tdrawable,self.color,True)
        self.MaskProtection(self.timg, self.tdrawable )

    def AdditiveMaskProtection(self):
        ''' 
        example with Green:
            m = Min( 1, R+B )
            G' = G×(1 — a)×(1 — m) + m×G (same as Maximum Mask)
        '''
        self.Add2Composantes( self.timg,self.tdrawable,self.color )
        self.MaskProtection(self.timg, self.tdrawable )

    def AverageNeutralProtection(self):
        ''' 
        example with Green:
            m = 0.5×(R + B)
            G' = Min( G, m )
        '''
        layer_cpy = copy_layer(self.timg,self.tdrawable,"Color reduction" )
        layer_m = self.Average2Composantes( self.timg,layer_cpy,self.color)            
        gimp.layer_set_mode(layer_m,"DARKEN_ONLY" )
        layer = gimp.image_merge_down(self.timg, layer_m, "EXPAND_AS_NECESSARY" )
        gimp.layer_set_mode(layer,"HSL_COLOR" )

    def MaximumNeutralProtection(self):
        ''' 
        example with Green
            m = Max( R, B ) (same as Maximum Mask)
            G' = Min( G, m ) (same as Average Neutral)
        '''
        layer_cpy = copy_layer(self.timg,self.tdrawable,"Color reduction" )
        layer_m =self.Max2Composantes( self.timg,layer_cpy,self.color)        
        gimp.layer_set_mode(layer_m,"DARKEN_ONLY" )
        layer = gimp.image_merge_down(self.timg, layer_m, "EXPAND_AS_NECESSARY" )
        gimp.layer_set_mode(layer,"HSL_COLOR" )
        
    # --------------------------------------------------------------------------            
    def Extract1Composante( self, img, drw, idx_color ) :
        if idx_color == 0 : # vert
            mixChan=(0.0, 1.0, 0.0)
        if idx_color == 1 : # red
            mixChan=(1.0, 0.0, 0.0)
        if idx_color == 2 : # blue
            mixChan=(0.0, 0.0, 1.0)

        layer_c=self.MixComposantes(img, drw, idx_color, mixChan, "layer c", True)
        return layer_c   
        
    def Average2Composantes( self, img, drw, idx_color, coef=0.5 ) :
        if idx_color == 0 : # vert
            mixChan=(coef, 0.0, coef)
        if idx_color == 1 : # red
            mixChan=(0.0, coef, coef)
        if idx_color == 2 : # blue
            mixChan=(coef, coef, 0.0)

        layer_m=self.MixComposantes(img, drw, idx_color, mixChan, "layer m")
        return layer_m
        
    def Add2Composantes( self, img, drw, idx_color, coef=1.0 ) :
        if idx_color == 0 : # vert
            mixChan1=(coef, 0.0, 0.0 )
            mixChan2=( 0.0, 0.0, coef)
        if idx_color == 1 : # red
            mixChan1=(0.0, coef, 0.0 )
            mixChan2=(0.0, 0.0 , coef)
        if idx_color == 2 : # blue
            mixChan1=(coef, 0.0 , 0.0)
            mixChan2=(0.0 , coef, 0.0)

        self.MixComposantes(img, drw, idx_color, mixChan1, "layer1 m")
        layer2=self.MixComposantes(img, drw, idx_color, mixChan2, "layer2 m")
        gimp.layer_set_mode(layer2,"ADDITION" )
        layer_m = gimp.image_merge_down(self.timg, layer2, "EXPAND_AS_NECESSARY" )
        return layer_m

    def Max2Composantes( self, img, drw, idx_color, clr_chan=False ) :
        Ronly=(1.0, 0.0, 0.0)
        Gonly=(0.0, 1.0, 0.0)
        Bonly=(0.0, 0.0, 1.0)

        if idx_color == 0 : # vert
            mixChan=Ronly
        if idx_color == 1 : # red
            mixChan=Gonly
        if idx_color == 2 : # blue
            mixChan=Ronly
        self.MixComposantes(img, drw, idx_color, mixChan, "layer m" ,clr_chan )

        if idx_color == 0 : # vert
            mixChan=Bonly
        if idx_color == 1 : # red
            mixChan=Bonly
        if idx_color == 2 : # blue
            mixChan=Gonly
            
        layer2 = self.MixComposantes(img, drw, idx_color, mixChan, "layer2",clr_chan )
        gimp.layer_set_mode(layer2,"LIGHTEN_ONLY" )
        layer_m = gimp.image_merge_down(img, layer2, "EXPAND_AS_NECESSARY" )
        return layer_m

    def MixComposantes(self, img, drw, idx_color, mixChan, layername="layer mix", clr_chan=False ) :
        layer = gimp.layer_copy(drw,False)
        gimp.item_set_name(layer,layername )
        gimp.image_insert_layer( img,layer, 0,-1)
        gimp.image_set_active_layer( img, layer)

        if clr_chan : 
            mixR=(0.0, 0.0, 0.0)
            mixG=mixR
            mixB=mixR
        else:
            mixR=(1.0, 0.0, 0.0)
            mixG=(0.0, 1.0, 0.0)
            mixB=(0.0, 0.0, 1.0)
            
        if idx_color == 0 : # vert
            mixG=mixChan
        if idx_color == 1 : # red
            mixR=mixChan
        if idx_color == 2 : # blue
            mixB=mixChan
        colors_channel_mixer(img, layer, False, mixR[0], mixR[1], mixR[2],  mixG[0], mixG[1], mixG[2],mixB[0], mixB[1], mixB[2] )
        return layer

    def newlayer(self,img,layer, idx_color,value):               
        if idx_color == 0 : # vert
            color=(0,value,0,0)
        if idx_color == 1 : # red
            color=(value,0,0,0)
        if idx_color == 2 : # blue
            color=(0,0,value,0)
            
        newlayer=create_layer(img, layer,"new layer ...", "NORMAL")      
        fill_layer(newlayer,color)
        return newlayer

    def MaskProtection(self, img, drw ) :
        ''' 
        example with Green
            G' = G×(1 — a)×(1 — m) + m×G
        
        simplification:
            G' = G×( (1 — a) + a×m )
        '''
        
        # a×m
        layer_a = self.newlayer( img, drw, self.color, self.amount)
        gimp.layer_set_mode(layer_a,"MULTIPLY" )
        layer_a_m = gimp.image_merge_down(img, layer_a, "EXPAND_AS_NECESSARY" ) 
        gimp.item_set_name(layer_a_m,"a×m" )
        # (1 — a) +a×m 
        layer_un_moins_a = self.newlayer( img, drw, self.color, 1.0-self.amount)
        gimp.layer_set_mode(layer_un_moins_a,"ADDITION" )
        layer_tmp = gimp.image_merge_down(img, layer_un_moins_a, "EXPAND_AS_NECESSARY" ) 
        gimp.item_set_name(layer_tmp,"(1 — a) + a×m " )
        # G' = G×( (1 — a) + a×m )
        layer_c = self.Extract1Composante( img,drw,self.color)
        gimp.layer_set_mode(layer_c,"MULTIPLY" )
        layer = gimp.image_merge_down(img, layer_c, "EXPAND_AS_NECESSARY" )
        gimp.item_set_name(layer,"G×( (1 — a) + a×m )" )
        
        layer = self.MixComposantes(img, drw, self.color, (0.0,0.0,0.0), layername="layer mix", clr_chan=False )
        gimp.layer_set_mode(layer,"ADDITION" )
        layer = gimp.image_merge_down(img, layer, "EXPAND_AS_NECESSARY" )
        gimp.item_set_name(layer,"Color reduction" )
        gimp.layer_set_mode(layer,"HSL_COLOR" )
        
        return layer

app=CColorNoiseReduction()
app.CreerPluginScm()
app.register()
