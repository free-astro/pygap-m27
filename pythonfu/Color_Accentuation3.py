#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,bullet,menu_color,init_language
from pyGapM27 import info_right0 as info_right
init_language()

import gp2_func as gimp
from gp2_plugin import CPlugin
from plug_in    import decompose,drawable_compose
from libTools   import check_compatibilty,copy_layer_to_image

class CColorAccentuation3( CPlugin):
    def __init__(self):
        shelp = ( _("Color accentuation using the layers A,B in the LAB mode")
              , bullet + _("Transform RGB image to LAB image")
              , bullet + _("Copy layer A (or B) ")
              , bullet + _("set the overlay mode ")
              , bullet + _("set the opacity level ")
              , bullet + _("Transform LAB image to RGB image") )
        params = [
            self.Param("FLOAT", 'opacity', _("Opacity"), 0.0),
            self.Param("INT32", 'layers' , _("Layers") ,  0 )]
        gui = {
            'opacity' : { "BOX": "SF-ADJUSTMENT", "VALBOX": '(list 8 1 50  0.1 1 1 0 )' } ,
            'layers'  : { "BOX": "SF-OPTION"    , "VALBOX": '(list "Layer A" "Layer B" "Layer A & B" )'} }

        CPlugin.__init__( self,
            Pkg+"ColorAccent3"                                , # name
            _("Color accentuation using the overlay on LAB layers") , # blurb
            shelp                                             , # help
            info_right                                        , # author,copyright, ...
            _("Accentuation using the overlay on LAB layers")     , # menu_label
            menu_color                                        , # menu path
            "RGB*, GRAY*"                                     , # image_types
            params                                            , # params
            gui, dir_scriptfu                                 ) # gui

    def Processing(self, timg, tdrawable):
        (opacity, layers) = self.get_parameters_value()

        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr
        
        gimp.image_undo_group_start(timg)
        
        layers=layers+1
        # 1) decompose en  une image LAB
        ret = decompose(timg, tdrawable, "LAB", True )
        if ret is None :
            return ( "EXECUTION_ERROR", "Abort plug_in.decompose()" )
        img_LAB=ret[1]
        
        # 2) recupere les calques  A et B
        layerL, layerA, layerB = gimp.get_layers(img_LAB)

        # 3) Applique  l'overlay sur le calque A
        if (layers & 1) == 1:
            layerA = self.ApplyOverlay(img_LAB,layerA,opacity)

        # 4) Applique  l'overlay sur le calque B
        if (layers & 2) == 2:
            layerB = self.ApplyOverlay(img_LAB,layerB,opacity)

        # 5) reconstitution de l'image RGB
        ret = drawable_compose( img_LAB, layerL, layerA, layerB, -1, "LAB" )
        if ret is None :
            return ( "EXECUTION_ERROR", "Abort plug_in.drawable_compose()" )
        img_RGB    = ret[1]
        layer_RGB = gimp.get_layers(img_RGB)[0]

        # 6) copie du calque RGB dans l'image img
        copy_layer_to_image(img_RGB, layer_RGB, timg, "Color Accentuation3 layer", -1 )
        
        # 7) destruction des images LAB inutilisees
        gimp.image_delete(img_LAB)

        gimp.image_undo_group_end(timg)
        gimp.displays_flush()

        return ( "SUCCESS","")
    
    def ApplyOverlay(self, img, layer, opacity ):
        layer_cpy = gimp.layer_copy(layer,True)
        gimp.image_set_active_layer( img, layer)
        gimp.image_insert_layer(img,layer_cpy, 0, -1)
        gimp.layer_set_mode(layer_cpy,'OVERLAY')
        gimp.layer_set_opacity(layer_cpy,opacity)
        return gimp.image_merge_down( img, layer_cpy, "EXPAND_AS_NECESSARY")
        
app=CColorAccentuation3()
app.CreerPluginScm()
app.register()
