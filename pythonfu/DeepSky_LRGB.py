#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,bullet,menu_composit,init_language
from pyGapM27 import info_right0 as info_right
init_language()

import gp2_func as gimp
from gp2_plugin    import CPlugin
from libTools      import check_compatibilty
from libLayerTools import GetLuminanceFromRGBImage,GetMix2layers

class CLRGB( CPlugin):
    def __init__(self):
        shelp = ( _("Compose a L layer with a RGB layer")
              , bullet + _("Pourcent")+ " L_"+ _("RGB") + " " + _("in") + " L" )
        params = [ self.Param("FLOAT", "mixLrvbL", _("mixing rate = L+Lrvb "), 0.0)]
        gui = { "mixLrvbL": self.Box("SF-ADJUSTMENT", '(list 30 0 100  1 10 1 0 )' ) }

        CPlugin.__init__( self,
            Pkg+"LRGB"                            , # name
            _("Compose L+RGB") + " - experimental", # blurb
            shelp                                 , # help
            info_right                            , # author,copyright, ...
            _("Compose L+RGB") + " - experimental", # menu_label
            menu_composit                         , # menu path
            "RGB*"                                , # image_types
            params                                , # params
            gui, dir_scriptfu                     ) # gui

    def Processing(self, timg, tdrawable):
        (mixLrvbL,) = self.get_parameters_value()

        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr

        gimp.image_undo_group_start(timg)

        # Layer ord: Top=L, Bottom=RGB
        layers=gimp.get_layers(timg)
        layerL  = layers[0]
        layerRGB= layers[1]
        gimp.image_set_active_layer( timg, layerL )

        # ----------------------------------------------------------------------
        # (1) Recupere la luminance synthétique du RGB
        layerLs = GetLuminanceFromRGBImage(timg, layerRGB )

        # ----------------------------------------------------------------------
        # (2) creer L+%Ls
        layerLLs = GetMix2layers(timg, layerL, layerLs, mixLrvbL, "L+%0.0f%%Ls"%(mixLrvbL) )

        # ----------------------------------------------------------------------
        # (3)  creer des images intermédaires
        gimp.image_set_active_layer( timg, layerRGB)
        layer_grp=gimp.layer_group_new(timg)
        gimp.image_insert_layer(timg,layer_grp, None,-1)
        gimp.item_set_name(layer_grp,"LRGB: intermediate layers")
        gimp.image_reorder_item(timg,layerRGB, layer_grp,-1)
        gimp.image_reorder_item(timg,layerL, layer_grp,-1)
        gimp.image_reorder_item(timg,layerLs, layer_grp,-1)
        gimp.item_set_visible( layer_grp, False)
        gimp.item_set_expanded( layer_grp, False)
        
        # Agence  les couches L et RGB
        layerRGB_cpy = gimp.layer_copy(layerRGB,True)
        gimp.item_set_name(layerRGB_cpy,"RGB" )
        gimp.image_insert_layer(timg,layerRGB_cpy, None,1)
        gimp.image_set_active_layer( timg, layerLLs)
        gimp.layer_set_mode(layerLLs,"LUMINANCE" )
        gimp.item_set_name(layerLLs,"LUMINANCE")

        gimp.image_undo_group_end(timg)
        gimp.displays_flush()

        return ( "SUCCESS","")

app=CLRGB()
app.CreerPluginScm()
app.register()
