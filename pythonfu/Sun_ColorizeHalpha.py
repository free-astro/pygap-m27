#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,bullet,menu_sun,init_language
from pyGapM27 import info_right0 as info_right
init_language()

import gp2_func as gimp
from gp2_plugin    import CPlugin
from libTools      import check_compatibilty,colorize_image

class CColorizeHalphaSun( CPlugin):
    def __init__(self):
        shelp = ( _("Colorize a Halpha monochrome solar image")
              , bullet + _("Set the level of each layer (RGB)")
              , bullet + _("Set the blur level of the color layer") 
              , bullet + _("Set the Contrast level of the color layer") 
              , bullet + _("Set or unset a negatif mode") 
              )
        params = [
               self.Param("FLOAT", 'ValueLevel', _("Value level")    , 1.00),
               self.Param("FLOAT", 'RedLevel'  , _("Red level")      , 1.30),
               self.Param("FLOAT", 'GreenLevel', _("Green level")    , 0.60),
               self.Param("FLOAT", 'BlueLevel' , _("Blue level")     , 0.20),
               self.Param("FLOAT", 'Blur'      , _("Blur level")     , 2.50),
               self.Param("FLOAT", 'Contrast'  , _("Contrast level") , 0.0 ),
               self.Param("INT32", "Reverse"   , _("Negatif picture"), 0   )
               ]
        gui = {
            'ValueLevel' : self.Box( "SF-ADJUSTMENT", '(list %f 0.1 2.15  0.01 0.1 2 0 )' ) ,
            'RedLevel'   : self.Box( "SF-ADJUSTMENT", '(list %f 0.0 2.15  0.01 0.1 2 0 )' ) ,
            'GreenLevel' : self.Box( "SF-ADJUSTMENT", '(list %f 0.0 2.15  0.01 0.1 2 0 )' ) ,
            'BlueLevel'  : self.Box( "SF-ADJUSTMENT", '(list %f 0.0 2.15  0.01 0.1 2 0 )' ) ,
            'Blur'       : self.Box( "SF-ADJUSTMENT", '(list %f 0.0 10.0  0.01 0.1 2 0 )' ) ,
            'Contrast'   : self.Box( "SF-ADJUSTMENT", '(list %f -0.5 0.5  0.01 0.1 2 0 )' ) ,
            'Reverse'    : self.Box( "SF-TOGGLE"    , 'FALSE' )}

        CPlugin.__init__( self,
            Pkg+"ColorizeHalphaSun"      , # name
            _("Colorize Halpha Sun")     , # blurb
            shelp                        , # help
            info_right                   , # author,copyright, ...
            _("Colorize Halpha Sun")     , # menu_label
            menu_sun +"/"+ _("Colorize") , # menu path
            "RGB*, GRAY*"                , # image_types
            params                       , # params
            gui, dir_scriptfu            ) # gui

    def Processing(self, timg, tdrawable):
        ( ValueLevel, RedLevel, GreenLevel, BlueLevel, Blur, Contrast, Reverse ) = self.get_parameters_value()

        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr        

        gimp.image_undo_group_start(timg)
        
        if Reverse :  gimp.drawable_invert( tdrawable, False )              
        colorize_image(timg,tdrawable, ValueLevel, RedLevel, GreenLevel, BlueLevel, Blur)
        gimp.drawable_brightness_contrast( gimp.get_layers(timg)[0], 0.0, Contrast )

        gimp.image_undo_group_end(timg)
        gimp.displays_flush()
        
        self.CreerPluginScm(force=True)

        return ( "SUCCESS","")


app=CColorizeHalphaSun()
app.CreerPluginScm()
app.register()
