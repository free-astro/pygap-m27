#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,bullet,menu_background,init_language
from pyGapM27 import info_right0 as info_right
init_language()

import gp2_func as gimp
from gp2_plugin import CPlugin
from libHisto   import CHisto
from libTools   import check_compatibilty

class CDarkBalance( CPlugin):
    def __init__(self):
        shelp = ( _("Calculate the dark Balance on a deep sky image")
              , bullet + _("Select the deep sky area")
              , bullet + _("Launch the plugin") )
        params = [
            self.Param("INT32", 'iteration', _("Iteration"), 0),
            self.Param("INT32", 'histo'    , _("Histogram"), 0)]
        gui = {
            'iteration': self.Box("SF-ADJUSTMENT", '(list 3 1 10  1   1 0 0 )') ,
            'histo'    : self.Box("SF-TOGGLE"    , 'FALSE') }

        CPlugin.__init__( self,
            Pkg+"DarkBalance"                    , # name
            _("Dark Balance on a deep sky image"), # blurb
            shelp                                , # help
            info_right                           , # author,copyright, ...
             _("Dark Balance")                   , # menu_label
            menu_background                      , # menu path
            "RGB*, GRAY*"                        , # image_types
            params                               , # params
            gui, dir_scriptfu                    ) # gui

    def Processing(self,timg, tdrawable):
        (iteration, histo) = self.get_parameters_value()

        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr

        gimp.image_undo_group_start(timg)

        gimp.progress_init( "Dark Balance" )

        gimp.progress_update(0.1)

        bLisse = True
        lissage= 7

        if gimp.drawable_is_rgb(tdrawable):
            self.algo_rgb(timg, tdrawable, iteration,bLisse, lissage )
        else:
            self.algo_gray(timg, tdrawable, bLisse, lissage )
        gimp.progress_update(0.75)

        if histo :
            chisto = CHisto()
            chisto.Algo( timg, tdrawable, MsgProgress="(*) ")
            chisto.Lissage(demi_prof=1 )
            #chisto.Display( timg, tdrawable, bLisse=True )
            chisto.Display( timg, tdrawable, bMaxLisse=True )

        gimp.progress_update(1.0)
        gimp.progress_end()

        gimp.image_undo_group_end(timg)
        gimp.displays_flush()

        return ( "SUCCESS","")

    def algo_rgb(self,timg, tdrawable, iteration,bLisse = False, lissage=3):
        chisto = CHisto()
        delta1 = 1.0
        delta2 = 1.0
        cnt    = 0
        bb = gimp.selection_bounds(timg)[1]
        while  (round(delta1) > 0.0) or (round(delta2)>0.0) :
            if bb[0] :
                gimp.image_select_rectangle( timg, "REPLACE", bb[1],bb[2], bb[3]-bb[1], bb[4]-bb[2] )
            chisto.Algo( timg, tdrawable, MsgProgress="(%d) " %(cnt) )
            if bLisse :
                chisto.Lissage(demi_prof=lissage )
            #( keystr, keystr1,delta1, keystr2, delta2 ) = chisto.GetPicDelta( bLisse )
            ( keystr, keystr1,delta1, keystr2, delta2 ) = chisto.GetDemiPicGaucheDelta( bLisse  )
            if keystr == None :
                keystr="Red"
                break
            nb_bins  = chisto.GetNbBins()
            minvalue = chisto.GetMinValue( bLisse )
            maxhisto = chisto.GetMaxhisto( bLisse )
            print( minvalue )
            print( maxhisto[keystr][0] )
            if float(maxhisto[keystr][0]-minvalue)/float(nb_bins) < 0.1 :
                minvalue = maxhisto[keystr][0]-0.1*nb_bins
                minvalue = max( minvalue, 0.0)

            minvalue = float(minvalue) / float(nb_bins)
            print( minvalue )

            gimp.drawable_levels(tdrawable, keystr1, delta1/100.0,1.0,False, 1.0,0.0, 1.0,False)
            gimp.drawable_levels(tdrawable, keystr2, delta2/100.0,1.0,False, 1.0,0.0, 1.0,False)
            gimp.drawable_levels(tdrawable, "VALUE", minvalue, 1.0, False, 1.0, 0.0, 1.0, False)
            cnt += 1
            if cnt >= iteration :
                break

    def algo_gray(self,timg, tdrawable, bLisse = False, lissage=3):
        chisto = CHisto()
        chisto.Algo( timg, tdrawable)
        if bLisse :
            chisto.Lissage(demi_prof=lissage )
        keystr="Value"
        nb_bins  = chisto.GetNbBins()
        minvalue = chisto.GetMinValue( bLisse )
        maxhisto = chisto.GetMaxhisto( bLisse )
        print( minvalue )
        print( maxhisto[keystr][0] )
        if float(maxhisto[keystr][0]-minvalue)/float(nb_bins) < 0.1 :
            minvalue = maxhisto[keystr][0]-0.1*nb_bins
            minvalue = max( minvalue, 0.0)

        minvalue = float(minvalue) / float(nb_bins)
        print( minvalue )
        gimp.drawable_levels(tdrawable, "VALUE", minvalue,1.0,False, 1.0, 0.0, 1.0,False)

app=CDarkBalance()
app.CreerPluginScm()
app.register()
