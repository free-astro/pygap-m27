#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
#
#   Python-Set-Dark-Sky - Set the image dark sky at chosen level.
#   Copyright (C) 2014  Bill Smith
#

import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,bullet,menu_background,init_language
from pyGapM27 import info_right0 as info_right
init_language()

import gp2_func  as gimp
from gp2_plugin    import CPlugin
from libTools      import check_compatibilty
from libTools      import copy_layer,create_layer,fill_layer
from plug_in       import rgb_noise

class CSetBackground( CPlugin):    
    def __init__(self):
        shelp  = ( _("Set the image dark sky with optional random noise")
                 , bullet + _("In first, select a area")
                 , bullet + _("And next, launch the plugin") ) 
        params = [ self.Param("FLOAT", "sigma"   , _('RMS noise level')              , 0.0)]
        gui = { "sigma"   : self.Box("SF-ADJUSTMENT", '(list 0.05 0.0  25.00 0.1 1.0 4 1 )')}

        CPlugin.__init__( self,
            Pkg+"SetBackground"     , # name
            _("Set the background") , # blurb
            shelp                   , # help
            info_right              , # author,copyright, ...
            _("Set the background") , # menu_label
            menu_background         , # menu path
            "RGB*"                  , # image_types
            params                  , # params
            gui, dir_scriptfu       ) # gui

    def Processing(self, timg, tdrawable):
        ''' This GIMP plugin sets the image dark sky.

        Parameters:
            timg     : The currently selected image.
            tdrawable: The layer of the currently selected image.
            sigma    : Root Mean Square of noise.
        '''
        (img, layer) = (timg, tdrawable)
        (sigma, ) = self.get_parameters_value()     

        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr        

        bb = gimp.selection_bounds(timg)[1]
        if bb[0] == 0 :
            gimp.message( _("Select a area")
                      + "\n" + _("and next, launch again the function") )
            return ( "SUCCESS","")
        
        gimp.image_select_rectangle( timg, "REPLACE", bb[1],bb[2], bb[3]-bb[1], bb[4]-bb[2] )
        red   = gimp.drawable_histogram(tdrawable, "RED"  ,0.0,1.0)[1][2]
        green = gimp.drawable_histogram(tdrawable, "GREEN",0.0,1.0)[1][2]
        blue  = gimp.drawable_histogram(tdrawable, "BLUE" ,0.0,1.0)[1][2]

        rgba_bkg = [red,green,blue,0]
        
        gimp.context_push()
        gimp.image_undo_group_start(img)
        gimp.selection_none(timg)
                      
        #copy_layer(img, layer , _("background")  , -1)
        mask_bkg = create_layer(img, layer,"Mask ...", "NORMAL", -1 )
        fill_layer(mask_bkg,rgba_bkg)
        rgb_noise( img, mask_bkg, True, False, sigma, sigma, sigma, 0)
        gimp.layer_set_mode(mask_bkg, "LIGHTEN_ONLY")
        #gimp.image_merge_down(img, mask_bkg, "EXPAND_AS_NECESSARY" )        
        
        gimp.image_undo_group_end(img)       
        gimp.context_pop()
        gimp.displays_flush()       
        
        return ( "SUCCESS", "")
    
app=CSetBackground()
app.CreerPluginScm()
app.register()
