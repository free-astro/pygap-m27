#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
#
#   Python-Set-Dark-Sky - Set the image dark sky at chosen level.
#   Copyright (C) 2014  Bill Smith
#

import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,menu_background,init_language
from pyGapM27 import info_right1 as info_right
init_language()

import gp2_func  as gimp
from gp2_plugin    import CPlugin
from libTools      import check_compatibilty
from libTools      import copy_layer,create_layer,fill_layer
from plug_in       import rgb_noise

class CSetDarkSky( CPlugin):    
    def __init__(self):
        shelp  = _("Set the image dark sky with optional random noise") 
        params = [
            self.Param("FLOAT", "old_dark", _('Current sky dark level')+" (%)", 0.0),
            self.Param("FLOAT", "new_dark", _('Set new sky dark level')+" (%)", 0.0),
            self.Param("INT32", "noise"   , _('Add random noise')             , 0.0),
            self.Param("FLOAT", "sigma"   , _('RMS noise level')              , 0.0)]
        gui = {
            "old_dark": self.Box("SF-ADJUSTMENT", '(list 1.25 0.0 100.00 0.1 1.0 4 1 )'),
            "new_dark": self.Box("SF-ADJUSTMENT", '(list 0.75 0.0 100.00 0.1 1.0 4 1 )'),
            "noise"   : self.Box("SF-TOGGLE"    , 'TRUE'                               ),               
            "sigma"   : self.Box("SF-ADJUSTMENT", '(list 0.05 0.0  25.00 0.1 1.0 4 1 )')}

        CPlugin.__init__( self,
            Pkg+"SetDarkSky"      , # name
            _("Set the dark sky") , # blurb
            shelp                 , # help
            info_right            , # author,copyright, ...
            _("Set the dark sky") , # menu_label
            menu_background       , # menu path
            "RGB*"                , # image_types
            params                , # params
            gui, dir_scriptfu     ) # gui

    def Processing(self, timg, tdrawable):
        ''' This GIMP plugin sets the image dark sky.

        Parameters:
            timg     : The currently selected image.
            tdrawable: The layer of the currently selected image.
            linear   : Set the linear mode else the log mode (10.log10()
            old_dark : The current dark level
            new_dark : The required sky dark point.
            noise    : Option to include random noise.
            sigma    : Root Mean Square of noise.
        '''
        (img, layer) = (timg, tdrawable)
        ( old_dark, new_dark, noise, sigma) = self.get_parameters_value()     

        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr        
               
        percent=0.01
        old_dark *= percent 
        new_dark *= percent 
        sigma    *= percent
            
        nChan=3
        if gimp.drawable_has_alpha(layer): nChan+=1

        sigma         = sigma * new_dark 
        rgba_old_dark = [old_dark,old_dark,old_dark,0.0]
        rgba_new_dark = [new_dark,new_dark,new_dark,0.0]
        delta=0.004 # ~1/256
        
        gimp.context_push()
        gimp.image_undo_group_start(img)
                      
        # layer signal + clipped noise
        mask_oldnoise = create_layer(img, layer,"Mask ...", "LIGHTEN_ONLY", -1 )
        fill_layer(mask_oldnoise,rgba_old_dark)             
        layer_signal_clipnoise = gimp.image_merge_down(img, mask_oldnoise, "EXPAND_AS_NECESSARY" )        

        # clipped noise suppression
        layer_threshold = copy_layer(img, layer_signal_clipnoise , "layer_threshold", -1 )
        gimp.drawable_threshold(layer_threshold,'RED'  ,rgba_old_dark[0]+delta,1.0)
        gimp.drawable_threshold(layer_threshold,'GREEN',rgba_old_dark[1]+delta,1.0)
        gimp.drawable_threshold(layer_threshold,'BLUE' ,rgba_old_dark[2]+delta,1.0)
        layer_threshold2 = gimp.layer_copy(layer_threshold, False )             
        gimp.layer_set_mode(layer_threshold, "MULTIPLY")
        layer=gimp.image_merge_down(img, layer_threshold, "EXPAND_AS_NECESSARY" )        
        
        # layer with new noise
        mask_newnoise = create_layer(img, layer,"Mask ...", "NORMAL", -1 )
        fill_layer(mask_newnoise,rgba_new_dark)             
        if noise : rgb_noise( img, mask_newnoise, True, False, sigma, sigma, sigma, 0)
        
        gimp.image_insert_layer(img, layer_threshold2, 0, -1)
        gimp.drawable_invert( layer_threshold2, True )         
        gimp.layer_set_mode(layer_threshold2, "MULTIPLY")
        layer_noise = gimp.image_merge_down(img, layer_threshold2, "EXPAND_AS_NECESSARY" ) 
        
        # add noise to signal
        gimp.layer_set_mode(layer_noise, "ADDITION")
        gimp.image_merge_down(img, layer_noise, "EXPAND_AS_NECESSARY" )
        
        gimp.image_undo_group_end(img)       
        gimp.context_pop()
        gimp.displays_flush()       
        
        return ( "SUCCESS", "")
    
app=CSetDarkSky()
app.CreerPluginScm()
app.register()
