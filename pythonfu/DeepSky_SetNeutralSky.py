#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
#
#   Python-Neutral-Dark-Sky - Set the image sky colour to neutral.
#   Copyright (C) 2015  Bill Smith
#

import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,menu_background,init_language
from pyGapM27 import info_right1 as info_right
init_language()

import gp2_func  as gimp
from gp2_plugin    import CPlugin
from libTools      import check_compatibilty
from libTools      import create_layer,fill_layer,getMaxDyn

class CSetNeutralSky( CPlugin):
    def __init__(self):
        shelp  =  _("Set the image sky to a neutral colour")
        params = [self.Param("STRING", "message", _('Message'), 0.0)]
        gui = {"message" :self.Box("SF-STRING", '"' +_("Set the image sky to a neutral colour") +'"')}

        CPlugin.__init__( self,
            Pkg+"SetNeutralSky"               , # name
            _("Set the sky colour to neutral"), # blurb
            shelp                             , # help
            info_right                        , # author,copyright, ...
            _("Set the sky colour to neutral"), # menu_label
            menu_background                   , # menu path
            "RGB*"                            , # image_types
            params                            , # params
            gui, dir_scriptfu                 ) # gui

    def Processing(self, timg, tdrawable):
        ''' This GIMP plugin sets the image sky to a neutral colour.

        Parameters:
        img    : The currently selected image.
        layer  : The layer of the currently selected image.
        '''
        (img, layer) = (timg, tdrawable)
        #(ksigma,) = self.get_parameters_value()

        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr

        nChan=3
        if gimp.drawable_has_alpha(layer): nChan+=1
        MaxDyn = getMaxDyn(img)

        gimp.context_push()
        gimp.image_undo_group_start(img)
        for ii in range(1): # iteration
            negligible,med_rgb,std_rgb = self.getHisto( img, layer, MaxDyn, ii )
            if negligible :
                break
            layer = self.ComputeWidthCoefficient(img, layer, std_rgb )
            
            negligible,med_rgb,std_rgb = self.getHisto( img, layer, MaxDyn, ii )
            layer = self.ComputeOffsetCoefficient(img, layer, med_rgb )

        self.getHisto( img, layer, MaxDyn, -1 )

        gimp.image_undo_group_end(img)
        gimp.context_pop()
        gimp.displays_flush()

        return ( "SUCCESS", "")

    def getHisto(self, img, layer, MaxDyn, no_iter ):
        _unused=(img) # unused parameter , suppress pylint warning
        (avg_red,std_red,med_red) = gimp.drawable_histogram(layer,"RED"  ,0.0,1.0)[1][0:3]
        (avg_grn,std_grn,med_grn) = gimp.drawable_histogram(layer,"GREEN",0.0,1.0)[1][0:3]
        (avg_blu,std_blu,med_blu) = gimp.drawable_histogram(layer,"BLUE" ,0.0,1.0)[1][0:3]
        #print( "avg:", avg_red,avg_grn,avg_blu)
        print( "median:", med_red,med_grn,med_blu)
        print( "std:", std_red,std_grn,std_blu)
        #avg_rgb = [ avg_red*MaxDyn, avg_grn*MaxDyn, avg_blu*MaxDyn]
        med_rgb = [ med_red*MaxDyn, med_grn*MaxDyn, med_blu*MaxDyn]
        std_rgb = [ std_red*MaxDyn, std_grn*MaxDyn, std_blu*MaxDyn]
        stddev_min= 0.01
        negligible = std_red < stddev_min or std_grn < stddev_min or std_blu < stddev_min
        if negligible and no_iter==0:
            msg =_("average") + ": RGB (%.3f,%.3f,%3f)\n"   %( avg_red,avg_grn,avg_blu)
            msg+=_("median ") + ": RGB (%.3f,%.3f,%3f)\n"   %( med_red,med_grn,med_blu)
            msg+=_("Std Dev") + ": RGB (%.3f,%.3f,%3f)\n\n" %( std_red,std_grn,std_blu)
            msg+=_("Error - Negligible colour variance detected") + " < %.3f"%(stddev_min,)
            gimp.message(msg)
        return ( negligible, med_rgb, std_rgb )

    def ComputeWidthCoefficient(self,  img, layer, std_rgb ):
        _unused=(layer) # unused parameter , suppress pylint warning
        ch_min=1
        ch_ref = 0
        for ii in range(len(std_rgb)):
            if ch_min> std_rgb[ii]:
                ch_ref = ii
                ch_min = std_rgb[ii]

        K_rgba=[]
        std_ref=std_rgb[ch_ref]
        for ii in range(len(std_rgb)):            
            K_rgba.append(1.0 - (std_rgb[ii]-std_ref)/std_ref/2.0)

        #print("K:", K_rgba)
        #print("")
        layer_Kx = create_layer(img, layer,"Mask Kx...","MULTIPLY", -1, 100.0)
        fill_layer(layer_Kx, K_rgba )
        return gimp.image_merge_down(img, layer_Kx, "EXPAND_AS_NECESSARY" )

    def ComputeOffsetCoefficient( self, img, layer, med_rgba ):
        _unused=(layer) # unused parameter , suppress pylint warning
        ch_max=0
        ch_ref = 0
        for ii in range(len(med_rgba)):
            if ch_max< med_rgba[ii]:
                ch_ref = ii
                ch_max = med_rgba[ii]

        offset_rgba=[]
        med_ref=med_rgba[ch_ref]
        for ii in range(len(med_rgba)):
            offset_rgba.append( med_ref-med_rgba[ii])

        #print("offset:", offset_rgba)
        #print("")
        layer_offset = create_layer(img, layer,"Mask OFFSET ...","ADDITION", -1, 100.0)
        fill_layer(layer_offset, offset_rgba )
        return gimp.image_merge_down(img, layer_offset, "EXPAND_AS_NECESSARY" )

app=CSetNeutralSky()
app.CreerPluginScm()
app.register()
