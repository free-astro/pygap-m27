#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,menu_filters,init_language
from pyGapM27 import info_right0 as info_right
init_language()

import gp2_func as gimp
from gp2_plugin    import CPlugin
from libTools      import check_compatibilty
from plug_in       import gegl_high_pass

class CHFfilters( CPlugin):
    def __init__(self):
        shelp = _("Applying HF filters iteratively using an object mask")
        params = [
            self.Param("FLOAT"   , "Contrast"    , _("Contrast")      , 0.0),
            self.Param("FLOAT"   , "InitialDev"  , _("Initial Dev")   , 0.0),
            self.Param("INT32"   , "Step"        , _("Step")          ,   0),
            self.Param("INT32"   , "Iteration"   , _("Filters number"),   0),
            self.Param("DRAWABLE", "ObjLayer"    , _("OBJECT DRAWING"),   0),
            self.Param("INT32"   , "Intermediare", _("Intermediate result"), 0 )]
        gui = {
            "Contrast"    : self.Box("SF-ADJUSTMENT", '(list 1 0.0 5.0  0.1  1 1 0 )' ) ,
            "InitialDev"  : self.Box("SF-ADJUSTMENT", '(list 4 1   100  1   10 1 0 )' ) ,
            "Step"        : self.Box("SF-ADJUSTMENT", '(list 8 1    50  1    5 0 0 )' ) ,
            "Iteration"   : self.Box("SF-ADJUSTMENT", '(list 3 1     8  1    2 0 0 )' ) ,
            "ObjLayer"    : self.Box("SF-DRAWABLE"  , '1'                             ) ,
            "Intermediare": self.Box("SF-TOGGLE"    , 'FALSE'                         ) }

        CPlugin.__init__( self,
            Pkg+"HFfilters"          , # name
            _("Iterative HF filters"), # blurb
            shelp                    , # help
            info_right               , # author,copyright, ...
            _("Iterative HF filters"), # menu_label
            menu_filters             , # menu path
            "RGB*, GRAY*"            , # image_types
            params                   , # params
            gui, dir_scriptfu        ) # gui

    def Processing(self, timg, tdrawable):
        ( Contrast, InitialDev, Step,
          Iteration, ObjLayer, Intermediate) = self.get_parameters_value()

        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr

        gimp.image_undo_group_start(timg)

        text=" ... " + _("Applying HF filters iteratively using an object mask") + " ..."
        gimp.progress_init( text)

        NbEtape=5
        NbEtapeTotal=NbEtape*Iteration

        NameLayer="HF Std="
        layer_cpy1 = gimp.layer_copy(tdrawable,True)
        gimp.image_insert_layer(timg,layer_cpy1, None,-1)
        for ii in range(int(Iteration)):
            gimp.progress_update((ii*NbEtape+0)/NbEtapeTotal)
            layer_cpy2 = gimp.layer_copy(tdrawable,True)
            gimp.image_insert_layer(timg,layer_cpy2, None,-1)
            gimp.layer_set_mode(layer_cpy2,"OVERLAY")

            gimp.progress_update((ii*NbEtape+1)/NbEtapeTotal)
            mask_layer = gimp.layer_create_mask(layer_cpy2, "BLACK")
            gimp.layer_add_mask(layer_cpy2,mask_layer)

            gimp.progress_update((ii*NbEtape+2)/NbEtapeTotal)
            gimp.image_set_active_layer( timg, ObjLayer )
            gimp.selection_all(timg)
            gimp.edit_copy(ObjLayer)
            gimp.image_set_active_layer( timg, ObjLayer )

            gimp.progress_update((ii*NbEtape+3)/NbEtapeTotal)
            float_sel = gimp.edit_paste(mask_layer,True)
            gimp.floating_sel_anchor(float_sel)
            gimp.layer_set_edit_mask( layer_cpy2, False )
            gimp.selection_none( timg )

            gimp.progress_update((ii*NbEtape+4)/NbEtapeTotal)
            gegl_high_pass(layer_cpy2,InitialDev,Contrast)
            layer_merge = gimp.image_merge_down(timg,layer_cpy2,"EXPAND_AS_NECESSARY")

            NameLayer=NameLayer + " " + str(InitialDev)
            gimp.item_set_name(layer_merge,NameLayer)
            InitialDev += Step
            if Intermediate :
                layer_cpy1 = gimp.layer_copy(layer_merge,True)
                gimp.image_insert_layer(timg,layer_cpy1, None,-1)

        gimp.image_undo_group_end(timg)
        gimp.displays_flush()

        return ( "SUCCESS","")

app=CHFfilters()
app.CreerPluginScm()
app.register()
