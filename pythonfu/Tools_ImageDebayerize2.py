#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
#
#   Python-Set-Dark-Sky - Set the image dark sky at chosen level.
#   Copyright (C) 2014  Bill Smith
#

import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,bullet,menu_imagetools,init_language
from pyGapM27 import info_right1 as info_right
init_language()

import gp2_func  as gimp
from gp2_plugin    import CPlugin
from libTools      import check_compatibilty,copy_layer
from plug_in       import drawable_compose,convmatrix

class CImageDebayerise2( CPlugin):
    def __init__(self):
        shelp  = ( _("De-Bayerize a raw image using the bilinear method")
                    , bullet + _("Before launch, set the bayer mask") )
        params = [ self.Param("INT32", "bayer_mask", _('Bayer mask'), 0) ]
        gui    = { "bayer_mask" : self.Box("SF-OPTION", '(list "[BGGR]" "[GBRG]" "[GRBG]" "[RGGB]" )')}

        CPlugin.__init__( self,
            Pkg+"ImageDebayerise2"   , # name
            _("De-Bayerize an image"), # blurb
            shelp                    , # help
            info_right               , # author,copyright, ...
            _("De-Bayerize image"  ) , # menu_label
            menu_imagetools+"/"+_("BAYER matrix") , # menu path
            "GRAY*"                  , # image_types
            params                   , # params
            gui, dir_scriptfu        ) # gui

    def Processing(self, timg, tdrawable):
        ''' This GIMP plugin de-Bayerizes a Bayerized image.

        Parameters:
            timg     : The currently selected image.
            tdrawable: The layer of the currently selected image.
            key      : The colour filter array as string e.g. [BGGR]
        '''
        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)      
        if cr != None : return cr        

        #(img, layer) = (timg, tdrawable)       
        img       = gimp.image_duplicate(timg)
        img_disp  = None
        #img_disp  = gimp.display_new(img)
        layer     = gimp.image_flatten(img)

        ( key, ) = self.get_parameters_value()

        CFA_GRID=[ "BGGR", "GBRG", "GRBG", "RGGB" ]
        cfa=CFA_GRID[key]

        gimp.context_push()
        gimp.image_undo_group_start(img)
        
        layer_pix={}

        firstG=True
        for pix in range(4):
            if cfa[pix]=="G" and firstG==False :
                continue
            pix2=-1
            if cfa[pix]=="G": 
                pix2=pix+2 if pix==0  else pix+1
                firstG=False
                
            layer_new = copy_layer(img,layer, "Pixels " + cfa[pix],-1)
            layer_pix[cfa[pix]]=self.extractColorPixel(img, layer_new, pix , pix2)
            self.interpo_bilinear(cfa[pix], img, layer_pix[cfa[pix]])
                
        ret=drawable_compose( img, layer_pix["R"], layer_pix["G"], layer_pix["B"], 0, "RGB"  )
        if ret is None :
            return ( "EXECUTION_ERROR", "Abort plug_in.drawable_compose()" )

        img_RGB=ret[1]
        gimp.image_undo_disable(img_RGB)
        layer_RVB = gimp.get_layers(img_RGB)[0]
        infilename = gimp.image_get_filename( timg )
        (outfilename,extension) = os.path.splitext(infilename)
        gimp.image_set_filename( img_RGB ,outfilename +  _("_RGB") + extension )
        gimp.item_set_name(layer_RVB, "RGB:" + os.path.basename(infilename) )
        gimp.display_new(img_RGB)
        gimp.image_undo_enable(img_RGB)
        
        gimp.image_undo_group_end(img)
        if img_disp == None :
            gimp.image_delete(img)
        else:
            gimp.display_delete(img_disp)
        
        gimp.context_pop()
        gimp.displays_flush()
        return ( "SUCCESS", "")
        
    def interpo_bilinear(self,name, img, drw):
        if name == "G" :
            layername="Green"
            matrix= (  0.00, 0.00, 0.00, 0.00, 0.00,
                       0.00, 0.00, 0.25, 0.00, 0.00,
                       0.00, 0.25, 1.00, 0.25, 0.00,
                       0.00, 0.00, 0.25, 0.00, 0.00,
                       0.00, 0.00, 0.00, 0.00, 0.00)
        else:
            layername="Red" if name == "R"  else "Blue"       
            matrix=(   0.00, 0.00, 0.00, 0.00, 0.00,
                       0.00, 0.25, 0.50, 0.25, 0.00,
                       0.00, 0.50, 1.00, 0.50, 0.00,
                       0.00, 0.25, 0.50, 0.25, 0.00,
                       0.00, 0.00, 0.00, 0.00, 0.00)
        Channels=( True, True, True, True, True )
        convmatrix( img, drw, 25, matrix, 0, 1.0, 0.0, 5, Channels, "WRAP" )
        gimp.item_set_name(drw, layername+" interpo " )    
          
    def extractColorPixel(self, img, layer, NoPixel,  NoPixel2):
        layer_mask =self.MaskPixel(img, layer, NoPixel,  NoPixel2)
        layer = gimp.image_merge_down(img, layer_mask, "EXPAND_AS_NECESSARY" )
        return layer

    def MaskPixel(self, img, layer, NoPixel,  NoPixel2):
        Black =(0.0,0.0,0.0,0.0)
        White =(1.0,1.0,1.0,0.0)

        width   = gimp.drawable_width(layer)
        height  = gimp.drawable_height(layer)
        drwtype = gimp.drawable_type(layer)
        layer_pix  = gimp.layer_new(img, "Pixel Selection",width, height,drwtype, 100, "MULTIPLY" )
        gimp.image_insert_layer(img,layer_pix, None,-1)
        for ii in range(4):
            x = ii % 2
            y = ii // 2
            color=Black
            if ii==NoPixel  : color=White
            if ii==NoPixel2 : color=White
            gimp.image_select_rectangle( img, "REPLACE",x, y, 1, 1)
            gimp.context_set_foreground( color )
            gimp.edit_fill(layer_pix,"FOREGROUND_FILL")

        gimp.image_select_rectangle( img, "REPLACE",0, 0, 2, 2)
        gimp.edit_copy(layer_pix)
        gimp.drawable_fill(layer_pix,"PATTERN_FILL")
        gimp.selection_none(img)
        return layer_pix        

app=CImageDebayerise2()
app.CreerPluginScm()
app.register()
