#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,bullet,menu_sun,init_language
from pyGapM27 import info_right0 as info_right
init_language()

import gp2_func as gimp
from gp2_plugin    import CPlugin
from libTools      import check_compatibilty,copy_layer,add_mask_with_selection
from libDisk       import CSelectDisk

class CProtuberanceMask( CPlugin):
    def __init__(self):
        shelp = (  _("Create a fusion mask to process the solar protuberances")  + ":"
              , bullet + _("In first, set a path with three points on the perimeter")
              , bullet + _("And next, launch the plugin") )
        params = [self.Param("FLOAT", "blur", _("Gaussian blur"), 2.5)]
        gui = { "blur"  : self.Box("SF-ADJUSTMENT", '(list 2.5 1 10  0.1 1 1 1 )' ) }

        CPlugin.__init__( self,
            Pkg+"ProtuberanceMask"           , # name
            _("Create a fusion mask to process the solar protuberances") , # blurb
            shelp                            , # help
            info_right                       , # author,copyright, ...
            _("Protuberances - fusion mask") , # menu_label
            menu_sun                         , # menu path
            "RGB*, GRAY*"                    , # image_types
            params                           , # params
            gui, dir_scriptfu                ) # gui

    def Processing(self, timg, tdrawable):
        ( blur_radius, ) = self.get_parameters_value()

        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr        

        gimp.context_push()
        gimp.image_undo_group_start(timg)

        app=CSelectDisk( timg, tdrawable )
        ret = app.Processing()
        if ret :
            gimp.image_undo_group_end(timg)
            gimp.context_pop()
            return ( "EXECUTION_ERROR",_("Aborted : selection solar disk"))
            
        gimp.selection_invert(timg)

        layer_cpy = copy_layer(timg,tdrawable,_("Protuberance Mask"),-1)
        add_mask_with_selection(timg,layer_cpy,blur_radius)
        
        gimp.image_undo_group_end(timg)
        gimp.context_pop()
        gimp.displays_flush()

        return ( "SUCCESS","")

app=CProtuberanceMask()
app.CreerPluginScm()
app.register()
