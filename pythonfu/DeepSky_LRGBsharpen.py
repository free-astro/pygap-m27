#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
#
#   Python-LRGB-Sharpen - Sharpens and colour enhances images.
#   Copyright (C) 2018  Bill Smith
#
import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,menu_enhancement,init_language
from pyGapM27 import info_right1 as info_right
init_language()

import gp2_func  as gimp
from gp2_plugin    import CPlugin
from libTools      import check_compatibilty
from libTools      import copy_layer,create_layer,fill_layer
from plug_in       import unsharp_mask,gauss_iir

class CLRGBsharpen( CPlugin):    
    def __init__(self):
        shelp  = _("Sharpen image using LRGB method of Okano/Dalby") 
        params = [
            self.Param("INT32", "desat" , _('Desaturation option')     , 0  ),
            self.Param("FLOAT", "radius", _('Radius of unsharp mask')  , 0.0),
            self.Param("FLOAT", "strong", _('Strength of unsharp mask'), 0.0),
            self.Param("FLOAT", "blur"  , _('Colour blur radius')      , 0.0),
            self.Param("FLOAT", "bright", _('Colour brightness factor'), 0.0)]
        gui = {
            "desat" : self.Box("SF-OPTION"    , '(list "Luminance" "Average" "Lightness" "Luma" )'),
            "radius": self.Box("SF-ADJUSTMENT", '(list   2.0  1.0  10.00  0.1  1.0 2 0 )'),
            "strong": self.Box("SF-ADJUSTMENT", '(list 100.0 50.0 100.00  1.0 10.0 2 0 )'),
            "blur"  : self.Box("SF-ADJUSTMENT", '(list   2.0  0.0  10.00  0.1  1.0 2 0 )'),
            "bright": self.Box("SF-ADJUSTMENT", '(list   1.0  1.0   2.00  0.1  1.0 2 0 )')}

        CPlugin.__init__( self,
            Pkg+"LRGBsharpen"                    , # name
            _("Sharpen image using LRGB method") , # blurb
            shelp                                , # help
            info_right                           , # author,copyright, ...
            _("LRGB sharpen")                    , # menu_label
            menu_enhancement                     , # menu path
            "RGB*"                               , # image_types
            params                               , # params
            gui, dir_scriptfu                    ) # gui

    def Processing(self, timg, tdrawable):
        ''' This GIMP plugin sharpens an image using Okano-Dalby LRGB method.

        Parameters:
        timg     : The currently selected image.
        tdrawable: The layer of the currently selected image.
        desat  : Desaturation option for luminance channel
        radius : Radius of unsharp mask gaussian blur
        strong : strength (opacity) of the unsharp mask
        blur   : Gaussian blurring of the colour layers
        bright : Brightness factor for colour layers
        '''
        (img, layer) = (timg, tdrawable)
        (desat, radius, strong, blur, bright) = self.get_parameters_value()

        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr
           
        key = ["LUMINANCE", "AVERAGE", "LIGHTNESS", "LUMINOSITY"]
        desaturate  = key[desat]
        mode_merge  = "EXPAND_AS_NECESSARY"
        basec       = 1.0 / bright
        bgcolor     = (basec, basec, basec)
        
        gimp.context_push()
        gimp.image_undo_group_start(img)

        layer1= copy_layer(img, layer , "Luminocity Mask", -1)
        gimp.drawable_desaturate(layer1, desaturate)
        gimp.layer_set_mode(layer1, "HSV_VALUE")
        
        unsharp_mask(img, layer1, radius, strong, 255)
        if blur >= 0.1: gauss_iir(img ,layer, blur, True, True)
        if bright >1.0:
            newlay = create_layer(img, layer,"Mask ...", "DIVIDE", -1, 100.0 )
            fill_layer(newlay,bgcolor)
            gimp.image_lower_item(img, newlay)
            gimp.image_merge_down(img, newlay, mode_merge)
        gimp.image_merge_down(img, layer1, mode_merge)

        gimp.image_undo_group_end(img)
        gimp.context_pop()
    
        return ( "SUCCESS", "")

app=CLRGBsharpen()
app.CreerPluginScm()
app.register()
