#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------

import sys
import os
import glob

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,init_language,stdout_clear
from pyGapM27 import info_right0 as info_right

init_language()
stdout_clear() 

import gp2_func as gimp
from gp2_plugin import CPlugin

class CGapM27InitMenu( CPlugin):
    def __init__(self):
        menu_path = "<Image>/Filters/py"+Pkg 
        shelp = _("Initializes the pyGapM27 menu")
        params= [self.Param( "INT32", 'confirm', _('Re-Initializes the pyGapM27 menu?'), 0)]
        gui   = { 'confirm' : self.Box( "SF-TOGGLE" , 'FALSE') }
        CPlugin.__init__( self,
            Pkg+"InitMenu"          , # name
            _("Menu Initialisation") , # blurb
            shelp                    , # help
            info_right               , # author,copyright, ...
            _("Menu Initialisation") , # menu_label
            menu_path                , # menu path
            ""                       , # image_types
            params                   , # params
            gui, dir_scriptfu        ) # gui

    def Processing(self, timg, tdrawable):
        _unused=(timg,tdrawable) # unused parameter , suppress pylint warning
        (confirm,) = self.get_parameters_value()
        if not confirm : return ( "SUCCESS","")
        
        files_py = glob.glob(os.path.join(os.path.dirname( __file__ ),"*.py"))
        for fname in files_py:
            print( "touch " +  fname)
            os.utime(fname, None)

        files_scm = glob.glob(os.path.join(dir_scriptfu,Pkg+"*.scm"))
        for fname in files_scm:
            try:
                os.unlink(fname)
                print( "remove " +  fname )
            except OSError as e:
                print("Error: %s : %s" % (fname, e.strerror))

        gimp.message(_("Now, restart GIMP"))
        return ( "SUCCESS","")

try:
    if not os.path.exists( dir_scriptfu ) :
        os.makedirs( dir_scriptfu )
except Exception as e :
    print(e)

app=CGapM27InitMenu()
app.CreerPluginScm()
app.register()
