#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,menu_filters,init_language
from pyGapM27 import info_right0 as info_right
init_language()

import gp2_func as gimp
from gp2_plugin    import CPlugin
from libTools      import check_compatibilty
from plug_in       import gegl_high_pass

class CHighPass( CPlugin):
    def __init__(self):
        shelp = _("Invokes gegl:high-pass (gimp 2.10)")
        params = [
            self.Param("FLOAT", 'stddev'  , _("Std deviation (radius =~ 3 std-dev)"),  0.0),
            self.Param("FLOAT", 'contrast', _("Contrast Range is [0.0 5.0]"),  0.0) ]
        gui = {
            'stddev'  : self.Box("SF-ADJUSTMENT", '(list 4 1 100 1   10 1 0 )') ,
            'contrast': self.Box("SF-ADJUSTMENT", '(list 1 1 5   0.1  1 1 0 )') }

        CPlugin.__init__( self,
            Pkg+"HighPass"                         , # name
            _("Invokes gegl:high-pass (gimp 2.10)"), # blurb
            shelp                                  , # help
            info_right                             , # author,copyright, ...
            _("Gegl:high-pass (gimp 2.10)")        , # menu_label
            menu_filters                           , # menu path
            "RGB*, GRAY*"                          , # image_types
            params                                 , # params
            gui, dir_scriptfu                      ) # gui

    def Processing(self, timg, tdrawable):
        _unused_ = (timg, tdrawable)
        (std_dev, contrast) = self.get_parameters_value()

        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr

        gegl_high_pass(tdrawable,std_dev,contrast)

        return ( "SUCCESS","")

app=CHighPass()
app.CreerPluginScm()
app.register()

