#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,menu_tools,init_language
from pyGapM27 import info_right1 as info_right
init_language()

import gp2_func  as gimp
from gp2_plugin    import CPlugin
from libTools      import check_compatibilty

class CGetStatisticImage( CPlugin):    
    def __init__(self):
        shelp  = ( _("get the image statistic by channel") + ":"
               , _("average, standard dev, median ") )
        params = [self.Param("STRING", "message" , _('Message'), 0.0)]
        gui = {"message" : self.Box("SF-STRING", '"'+_("get the image statistic by channel")+'"')}

        CPlugin.__init__( self,
            Pkg+"GetStatisticImage"     , # name
            _("Get the image statistic"), # blurb
            shelp                       , # help
            info_right                  , # author,copyright, ...
            _("Get the image statistic"), # menu_label
            menu_tools                  , # menu path
            "RGB*, GRAY*"               , # image_types
            params                      , # params
            gui, dir_scriptfu           ) # gui

    def Processing(self, img, layer):
        ''' This GIMP plugin sets the image black point automatically.

        Parameters:
        img     : The currently selected image.
        layer   : The layer of the currently selected image.
        '''
        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,img,layer)
        if cr != None : return cr        
        
        if gimp.drawable_is_rgb(layer):
            (avg_red,sig_red,med_red) = gimp.drawable_histogram(layer,"RED"  ,0.0,1.0)[1][0:3]
            (avg_grn,sig_grn,med_grn) = gimp.drawable_histogram(layer,"GREEN",0.0,1.0)[1][0:3]
            (avg_blu,sig_blu,med_blu) = gimp.drawable_histogram(layer,"BLUE" ,0.0,1.0)[1][0:3]            
            msg  =_("average") + " : RGB (%.4f, %.4f, %4f)\n" %( avg_red,avg_grn,avg_blu)
            msg +=_("median") + "  : RGB (%.4f, %.4f, %4f)\n" %( med_red,med_grn,med_blu)
            msg +=_("Std Dev") + " : RGB (%.4f, %.4f, %4f)\n" %( sig_red,sig_grn,sig_blu)
        else:
            (avg,sig,med) = gimp.drawable_histogram(layer,"VALUE" ,0.0,1.0)[1][0:3]            
            msg  =_("average") + " : %.4f\n" %( avg,)
            msg +=_("median") + "  : %.4f\n" %( med,)
            msg +=_("Std Dev") + " : %.4f\n" %( sig,)
            

        gimp.message(_("Statistic") + ":\n\n" + msg )
        return ( "SUCCESS", "")

app=CGetStatisticImage()
app.CreerPluginScm()
app.register()
