#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,menu_tools,init_language
from pyGapM27 import info_right0 as info_right
init_language()

from libGridSelection import CGridSelection

import gp2_func   as gimp
from gp2_plugin    import CPlugin
from libTools      import check_compatibilty

class CGridSelect( CPlugin):
    def __init__(self):
        shelp =  _("Make a regular grid selection")
        params = [
            self.Param("INT32", 'rayon'    , _("Size")          , 0),
            self.Param("INT32", 'nb_xpoint',  _("Point by line"), 0)]
        gui = {
            'rayon'    : self.Box("SF-ADJUSTMENT", '(list 200 20 400  1 10 0 0 )' ) ,
            'nb_xpoint': self.Box("SF-ADJUSTMENT", '(list  20 11 100  1  1 0 0 )' ) }

        CPlugin.__init__( self,
            Pkg+"GridSelection" , # name
             _("Grid selection"), # blurb
            shelp               , # help
            info_right          , # author,copyright, ...
             _("Grid selection"), # menu_label
            menu_tools          , # menu path
            "RGB*, GRAY*"       , # image_types
            params              , # params
            gui, dir_scriptfu   ) # gui

    def Processing(self, timg, tdrawable):
        (rayon, nb_xpoint) = self.get_parameters_value()

        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr        

        gimp.image_undo_group_start(timg)

        grid = CGridSelection( timg, tdrawable, nb_xpoint, rayon)
        grid.set( )

        gimp.image_undo_group_end(timg)
        gimp.displays_flush()

        return ( "SUCCESS","")

app=CGridSelect()
app.CreerPluginScm()
app.register()
