#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,bullet,menu_composit,init_language
from pyGapM27 import info_right0 as info_right
init_language()

import gp2_func as gimp
from gp2_plugin    import CPlugin
from libTools      import check_compatibilty
from libLayerTools import GetRedFromRGBImage,GetLuminanceFromRGBImage,GetMix2layers,ReplaceColorFromRGBImage 

class CHaLRGB( CPlugin):
    def __init__(self):
        shelp = ( _("Compose  L , Ha layers with a RGB layer")
              , bullet + _("Pourcent") +" Ha " + _("in") + " R "  + _("for the red layer")
              , bullet + _("Pourcent") +" R "  + _("in") + " Ha " + _("for the luminance layer")
              , bullet + _("Pourcent") +" Ls " + _("in") + " L "  + _("for the luminance layer")
              , bullet + _("Pourcent") +" Ha " + _("in") + " L "  + _("for the luminance layer") )
        params = [
            self.Param("FLOAT", "mixHaR", _("mixing rate = %R in Ha"), 0.0),
            self.Param("FLOAT", "mixRHa", _("mixing rate = %Ha in R"), 0.0),
            self.Param("FLOAT", "mixLLs", _("mixing rate = %Ls in L"), 0.0),
            self.Param("FLOAT", "mixLHa", _("mixing rate = %Ha in L"), 0.0)]
        gui = {
            "mixHaR"  : self.Box("SF-ADJUSTMENT", '(list 30 0 100  1 10 1 0 )' ) ,
            "mixRHa"  : self.Box("SF-ADJUSTMENT", '(list 30 0 100  1 10 1 0 )' ) ,
            "mixLLs"  : self.Box("SF-ADJUSTMENT", '(list  0 0 100  1 10 1 0 )' ) ,
            "mixLHa"  : self.Box("SF-ADJUSTMENT", '(list 30 0 100  1 10 1 0 )' ) }

        CPlugin.__init__( self,
            Pkg+"HaLRGB"                             , # name
            _("Compose Ha+L+RGB") + " - experimental", # blurb
            shelp                                    , # help
            info_right                               , # author,copyright, ...
            _("Compose Ha+L+RGB") + " - experimental", # menu_label
            menu_composit                            , # menu path
            "RGB*"                                   , # image_types
            params                                   , # params
            gui, dir_scriptfu                        ) # gui

    def Processing(self, timg, tdrawable):
        (mixHaR, mixRHa, mixLLs, mixLHa) = self.get_parameters_value()

        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr

        gimp.image_undo_group_start(timg)

        # Layer ord: Top=Ha, Middle=L, Bottom=RGB
        layers=gimp.get_layers(timg)
        layerHa, layerL, layerRGB = layers[0:3]
        gimp.image_set_active_layer( timg, layerHa)

        # ----------------------------------------------------------------------
        # (1) Recupere la luminance synthétique du RGB
        layerLs = GetLuminanceFromRGBImage(timg, layerRGB )

        # ----------------------------------------------------------------------
        # (2) Recupere la couche R du RGB
        layerR, images_RGB = GetRedFromRGBImage(timg, layerRGB )

        # ----------------------------------------------------------------------
        # (3) creer Ha+%R
        layerHaR = GetMix2layers(timg, layerHa, layerR, mixHaR, "Ha+%0.0f%%R"%(mixHaR) )

        # ----------------------------------------------------------------------
        # (4) creer R+%Ha
        layerRHa = GetMix2layers(timg, layerR, layerHa, mixRHa, "R+%0.0f%%Ha"%(mixRHa) )

        # ----------------------------------------------------------------------
        # (5) creer (L+%Ls+%Ha)
        img_cpy       = gimp.image_duplicate(timg)
        # 7 Couches = 0:R+%Ha,1:Ha+%R,2:RGB->R,3:RGB->Ls,4:Ha,5:L,6:RGB
        layers = gimp.get_layers( img_cpy)
        gimp.image_remove_layer(img_cpy,layers[6])
        gimp.image_remove_layer(img_cpy,layers[4])
        gimp.image_remove_layer(img_cpy,layers[2])
        gimp.image_remove_layer(img_cpy,layers[0])
        # maintenant: 3 Couches = 0:Ha+%R,1:RGB->Ls,2:L
        layers = gimp.get_layers( img_cpy)
        gimp.layer_set_opacity(layers[0],mixLHa)
        gimp.layer_set_opacity(layers[1],mixLLs)
        layerLLsHa = gimp.image_flatten(img_cpy)
        layerLLsHa_cpy = gimp.layer_new_from_drawable(layerLLsHa,timg)
        gimp.image_insert_layer(timg,layerLLsHa_cpy, None,-1)
        gimp.image_delete(img_cpy)
        gimp.image_set_active_layer( timg, layerLLsHa_cpy)
        gimp.item_set_name(layerLLsHa_cpy,"L+%0.0f%%Ls+%0.0f%%Ha"%(mixLLs,mixLHa))
        layerLLsHa = gimp.get_layers(timg)[0]
        # ----------------------------------------------------------------------

        # ----------------------------------------------------------------------
        # (6) creer RHaVB
        ReplaceColorFromRGBImage(timg, images_RGB, layerRHa, "R", "RHaVB" )

        # ----------------------------------------------------------------------
        # (7)  creer des images intermédaires
        gimp.image_set_active_layer( timg, layerRGB)
        layer_grp=gimp.layer_group_new(timg)
        gimp.image_insert_layer(timg,layer_grp, None,-1)
        gimp.item_set_name(layer_grp,"LHaRGB: intermediate layers")
        gimp.image_reorder_item(timg,layerRGB  , layer_grp,-1)
        gimp.image_reorder_item(timg,layerL    , layer_grp,-1)
        gimp.image_reorder_item(timg,layerHa   , layer_grp,-1)
        gimp.image_reorder_item(timg,layerLs   , layer_grp,-1)
        gimp.image_reorder_item(timg,layerR    , layer_grp,-1)
        gimp.image_reorder_item(timg,layerRHa  , layer_grp,-1)
        gimp.image_reorder_item(timg,layerHaR  , layer_grp,-1)
        gimp.item_set_visible(  layer_grp, False)
        gimp.item_set_expanded( layer_grp, False)

        # Agence  les couches LLsHa et RGB
        gimp.image_set_active_layer( timg, layerLLsHa)
        gimp.layer_set_mode(layerLLsHa,"LUMINANCE")
        gimp.item_set_name(layerLLsHa,"LUMINANCE")

        gimp.image_undo_group_end(timg)
        gimp.displays_flush()

        return ( "SUCCESS","")

app=CHaLRGB()
app.CreerPluginScm()
app.register()
