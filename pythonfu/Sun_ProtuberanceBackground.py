#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
import os
import traceback

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,bullet,menu_sun,init_language
from pyGapM27 import info_right0 as info_right
init_language()

import gp2_func as gimp
from gp2_plugin    import CPlugin
from libTools      import check_compatibilty,colorize_image,copy_layer
from plug_in       import wavelet_decompose

class CProtuberanceBackground( CPlugin):
    def __init__(self):
        shelp = ( _("equalizes the background of the protuberances")
              , bullet + _("Set opacity")
              , bullet + _("Merge or not the wavelet layers")
              )
        params = [
               self.Param("FLOAT", 'Opacity', _("Opacity level"), 0.75),
               self.Param("INT32", "MergeLayer", _("Merge Layers"), 1 )
               ]
        gui = {
            'Opacity' : self.Box( "SF-ADJUSTMENT", '(list %f 0.0 1  0.01 0.1 2 0 )' ) ,
            "MergeLayer": self.Box("SF-TOGGLE"    , 'TRUE'                            )
            }

        CPlugin.__init__( self,
            Pkg+"ProtuberanceBackground"    , # name
            _("Equalizes protuberances background"), # blurb
            shelp                           , # help
            info_right                      , # author,copyright, ...
            _("Protuberances - equalizes background"), # menu_label
            menu_sun                        , # menu path
            "RGB*, GRAY*"                   , # image_types
            params                          , # params
            gui, dir_scriptfu            ) # gui

    def Processing(self, timg, tdrawable):
        ( Opacity, MergeLayer ) = self.get_parameters_value()

        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr        

        gimp.image_undo_group_start(timg)
            
        wavelet_decompose(timg, tdrawable,scales=7,create_mask=False,create_group=False)
        
        layers=gimp.get_layers(timg)
        gimp.image_set_active_layer( timg, layers[7])
        layer_cpy0=copy_layer( timg,layers[7], _("Residual copy0"),-1)
        layer_cpy1=copy_layer( timg,layers[7], _("Residual copy1"), 6)
        layer_cpy2=copy_layer( timg,layers[7], _("Residual copy2"), 5)
        gimp.layer_set_mode(layer_cpy0,"MULTIPLY")
        gimp.layer_set_mode(layer_cpy1,"MULTIPLY")
        gimp.layer_set_mode(layer_cpy2,"MULTIPLY")
        
        gimp.layer_set_opacity(layer_cpy0,Opacity*100)
        gimp.layer_set_opacity(layer_cpy1,Opacity*66)
        gimp.layer_set_opacity(layer_cpy2,Opacity*33)
        
        if MergeLayer :
            layers=gimp.get_layers(timg)
            nBoucle=len(layers)-2
            for ii in range(nBoucle):
                gimp.image_merge_down(timg,layers[nBoucle-1-ii],"EXPAND_AS_NECESSARY")
                
            layers=gimp.get_layers(timg)
            gimp.item_set_name(layers[0],_("protuberances background"))
       
        gimp.image_undo_group_end(timg)
        gimp.displays_flush()
        
        self.CreerPluginScm(force=True)

        return ( "SUCCESS","")


app=CProtuberanceBackground()
app.CreerPluginScm()
app.register()
