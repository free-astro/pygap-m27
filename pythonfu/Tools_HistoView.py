#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,menu_tools,init_language
from pyGapM27 import info_right0 as info_right
init_language()

import gp2_func as gimp
from gp2_plugin    import CPlugin
from libTools      import check_compatibilty
from libHisto      import CHisto

class CHistoView( CPlugin):
    def __init__(self):
        shelp = _("Calculate the histogram of an image and display it as an image")
        params=[] 
        gui={}
        CPlugin.__init__( self,
            Pkg+"HistoView"    , # name
            _("Histogram View"), # blurb
            shelp              , # help
            info_right         , # author,copyright, ...
            _("Histogram View"), # menu_label
            menu_tools         , # menu path
            "RGB*, GRAY*"      , # image_types
            params             , # params
            gui, dir_scriptfu  ) # gui

    def Processing(self, timg, tdrawable):
        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)      
        if cr != None : return cr        

        gimp.image_undo_group_start(timg)

        gimp.progress_init( "Histogram view"  )
        chisto = CHisto()
        gimp.progress_update(0.1)
        chisto.Algo( timg, tdrawable)
        gimp.progress_update(0.5)
        chisto.Lissage(demi_prof=1 )
        gimp.progress_update(0.75)
        chisto.Display( timg, tdrawable, bMaxLisse=True)
        gimp.progress_update(1.0)
        gimp.progress_end()

        gimp.image_undo_group_end(timg)
        gimp.displays_flush()

        return ( "SUCCESS","")

app=CHistoView()
app.CreerPluginScm()
app.register()
