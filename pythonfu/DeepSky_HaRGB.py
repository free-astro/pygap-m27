#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,bullet,menu_composit,init_language
from pyGapM27 import info_right0 as info_right
init_language()

import gp2_func as gimp
from gp2_plugin    import CPlugin
from libTools      import check_compatibilty
from libLayerTools import GetRedFromRGBImage,GetMix2layers,ReplaceColorFromRGBImage

class CHaRGB( CPlugin):
    def __init__(self):
        shelp = ( _("Compose a") +" Ha "+ _("layer with a RGB layer")
              , bullet + _("Pourcent") +" Ha " + _("in") + " R "  + _("for the red layer")
              , bullet + _("Pourcent") +" R "  + _("in") + " Ha " + _("for the luminance layer") )
        params = [
            self.Param("FLOAT", "mixHaR", _("mixing rate = %R in Ha"), 0.0),
            self.Param("FLOAT", "mixRHa", _("mixing rate = %Ha in R"), 0.0)]
        gui = {
            "mixHaR": self.Box("SF-ADJUSTMENT", '(list 30 0 100  1 10 1 0 )' ) ,
            "mixRHa": self.Box("SF-ADJUSTMENT", '(list 30 0 100  1 10 1 0 )' ) }

        CPlugin.__init__( self,
            Pkg+"HaRGB"                            , # name
            _("Compose Ha+RGB") + " - experimental", # blurb
            shelp                                  , # help
            info_right                             , # author,copyright, ...
            _("Compose Ha+RGB") + " - experimental", # menu_label
            menu_composit                          , # menu path
            "RGB*"                                 , # image_types
            params                                 , # params
            gui, dir_scriptfu                      ) # gui

    def Processing(self, timg, tdrawable):
        (mixHaR, mixRHa) = self.get_parameters_value()

        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr

        gimp.image_undo_group_start(timg)

        # Layer ord: Top=Ha, Bottom=RGB
        layers=gimp.get_layers(timg)
        layerHa = layers[0]
        layerRGB= layers[1]
        gimp.image_set_active_layer( timg, layerHa)

        # ----------------------------------------------------------------------
        # (1) Recupere la couche R du RGB
        layerR, images_RGB = GetRedFromRGBImage(timg, layerRGB )

        # ----------------------------------------------------------------------
        # (2) creer Ha+%R
        layerHaR = GetMix2layers(timg, layerHa, layerR, mixHaR, "Ha+%0.0f%%R"%(mixHaR) )

        # ----------------------------------------------------------------------
        # (3) creer R+%Ha
        layerRHa = GetMix2layers(timg, layerR, layerHa, mixRHa, "R+%0.0f%%Ha"%(mixRHa) )

        # ----------------------------------------------------------------------
        # (4) creer RHaVB
        ReplaceColorFromRGBImage(timg, images_RGB, layerRHa, "R", "RHaVB" )

        # ----------------------------------------------------------------------
        # (5)  creer des images intermédaires
        gimp.image_set_active_layer( timg, layerRGB)
        layer_grp=gimp.layer_group_new(timg)
        gimp.image_insert_layer(timg,layer_grp, None,-1)
        gimp.item_set_name(layer_grp,"HaRGB: intermediate layers")
        gimp.image_reorder_item(timg,layerRGB, layer_grp,-1)
        gimp.image_reorder_item(timg,layerHa , layer_grp,-1)
        gimp.image_reorder_item(timg,layerR  , layer_grp,-1)
        gimp.image_reorder_item(timg,layerRHa, layer_grp,-1)
        gimp.item_set_visible(  layer_grp, False)
        gimp.item_set_expanded( layer_grp, False)

        # Agence  les couches HaR et RGB
        gimp.image_set_active_layer( timg, layerHaR)
        gimp.layer_set_mode(layerHaR,"LUMINANCE")
        gimp.item_set_name(layerHaR,"LUMINANCE HaR")
        gimp.image_reorder_item(timg,layerHaR, None,-1)

        gimp.image_undo_group_end(timg)
        gimp.displays_flush()

        return ( "SUCCESS","")

app=CHaRGB()
app.CreerPluginScm()
app.register()
