#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
#
#   Python_Merge_Layers - merge down all layers in the image with the
#   appropriate opacity
#   Copyright (C) 2014 Bill Smith
#
import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,menu_layertools,init_language
from pyGapM27 import info_right1 as info_right
init_language()

import gp2_func  as gimp
from gp2_plugin    import CPlugin
from libTools      import check_compatibilty

class CStackLayers( CPlugin):    
    def __init__(self):
        shelp  = _("Stack all layers in image with appropriate opacity")
        params = []
        gui    = {}

        CPlugin.__init__( self,
            Pkg+"StackLayers"     , # name
            _("Stack all layers") , # blurb
            shelp                 , # help
            info_right            , # author,copyright, ...
            _("Stack all layers") , # menu_label
            menu_layertools       , # menu path
            "RGB*"                , # image_types
            params                , # params
            gui, dir_scriptfu     ) # gui

    def Processing(self, timg, tdrawable):
        ''' Stacks all layers in image with appropriate opacity.

        Parameters:
        timg     : The currently selected image.
        tdrawable: The layer of the currently selected image.
        '''
        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr        
            
        gimp.image_undo_group_start(timg)
        layers = gimp.get_layers(timg)
        num = len(layers)
        n = 0
        # count visible layers
        for i in range(num):
            if (gimp.item_get_visible(layers[i]) == True):
                n += 1
        #  visible layers
        k=0
        for i in range(num):
            if (gimp.item_get_visible(layers[i]) == True):
                blend = int(1000 / (n - k)) / 10.0
                gimp.layer_set_opacity(layers[i], blend)
                gimp.layer_set_mode(layers[i],"NORMAL" )
                k += 1
        gimp.image_flatten(timg)
        layers = gimp.get_layers( timg ) 
        gimp.item_set_name(layers[0],"Merged image")
        gimp.image_undo_group_end(timg)

        return ( "SUCCESS", "")

app=CStackLayers()
app.CreerPluginScm()
app.register()
