#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,bullet,menu_sun,init_language
from pyGapM27 import info_right0 as info_right
init_language()

import gp2_func as gimp
from gp2_plugin    import CPlugin
from libTools      import check_compatibilty
from libDisk       import CSelectDisk

class CSelectSolarDisk( CPlugin):
    def __init__(self):
        shelp = (  _("Select the solar disk")  + ":"
              , bullet + _("In first, set a path with three points on the perimeter")
              , bullet + _("And next, launch the plugin") )
        params = [self.Param("INT32", "inverse", _("Inverse selection"), 0)]
        gui = {  "inverse" : self.Box("SF-TOGGLE", 'FALSE' )  }

        CPlugin.__init__( self,
            Pkg+"SelectSolarDisk"     , # name
            _("Select the solar disk"), # blurb
            shelp                     , # help
            info_right                , # author,copyright, ...
            _("Select Sun")           , # menu_label
            menu_sun                  , # menu path
            "RGB*, GRAY*"             , # image_types
            params                    , # params
            gui, dir_scriptfu         ) # gui

    def Processing(self, timg, tdrawable):
        ( inverse, ) = self.get_parameters_value()

        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr        

        app=CSelectDisk( timg, tdrawable )
        ret = app.Processing()
        if ret :
            return ( "EXECUTION_ERROR",_("Aborted : selection solar disk"))
            
        if  inverse :
            gimp.selection_invert(timg)

        return ( "SUCCESS","")

app=CSelectSolarDisk()
app.CreerPluginScm()
app.register()
