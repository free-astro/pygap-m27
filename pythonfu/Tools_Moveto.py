#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,bullet,menu_tools,init_language
from pyGapM27 import info_right0 as info_right
init_language()

import gp2_func as gimp
from gp2_plugin    import CPlugin
from libTools      import check_compatibilty,GetPointsVector

class CMoveto( CPlugin):
    def __init__(self):
        shelp = (  _("Select the layer motion vector ")  + ":"
              , bullet + _("In first, set a path with two points (motion vector)")
              , bullet + _("And next, launch the plugin") )
        params = [self.Param("INT32" , "ColorFill"       , _('Color Fill')         , 0  )]
        gui = {"ColorFill": self.Box( "SF-OPTION"    , '(list "' + _("Median color") + '" ' 
                                                       + '"' + _("Mean color")       + '" '
                                                       + '"' + _("Background color") + '" '
                                                       + '"' + _("Foreground color") + '" '
                                                       + '"' + _("Black") + '" '
                                                       + '"' + _("White") + '" '
                                                       + '"' + _("Transparent") + '" '
                                                       + '"' + _("None") + '" '
                                                       + ' )')}

        CPlugin.__init__( self,
            Pkg+"Moveto"      , # name
            _("Moveto")       , # blurb
            shelp             , # help
            info_right        , # author,copyright, ...
            _("Move to ")     , # menu_label
            menu_tools        , # menu path
            "RGB*, GRAY*"     , # image_types
            params            , # params
            gui, dir_scriptfu ) # gui

    def GetColor(self, tdrawable, colorfill):
        print("Color Fill:", colorfill, tdrawable)
        ListColorFill={"Median":0,"Mean":1,"Background":2,"Foreground":3,"Black":4,"White":5,"Transparent":6,"None":6}
        if colorfill == ListColorFill['Black']     : return (0,0,0,0)
        if colorfill == ListColorFill['White']     : return (1,1,1,0)
        if colorfill == ListColorFill['Background']: return gimp.context_get_background()[1]
        if colorfill == ListColorFill['Foreground']: return gimp.context_get_foreground()[1]
              
        ii = 2 if colorfill == ListColorFill['Median'] else 0     
        if colorfill == ListColorFill['Median'] or colorfill == ListColorFill['Mean'] : 
            if gimp.drawable_is_rgb(tdrawable):
                v_red = gimp.drawable_histogram(tdrawable,"RED"  ,0.0,1.0)[1][0:3]
                v_grn = gimp.drawable_histogram(tdrawable,"GREEN",0.0,1.0)[1][0:3]
                v_blu = gimp.drawable_histogram(tdrawable,"BLUE" ,0.0,1.0)[1][0:3]    
                print("Color", ii, (v_red[ii],v_grn[ii],v_blu[ii])) 
                return (v_red[ii],v_grn[ii],v_blu[ii],0)  
            else:
                v_grey = gimp.drawable_histogram(tdrawable,"VALUE" ,0.0,1.0)[1][0:3]
                print("Mono", ii, (v_grey[ii],v_grey[ii],v_grey[ii])) 
                return (v_grey[ii],v_grey[ii],v_grey[ii],0)
        if colorfill == ListColorFill['Transparent'] :
            if not gimp.drawable_has_alpha(tdrawable) :
                gimp.layer_add_alpha(tdrawable)
            return (0,0,0,1)
        return None
    
    def Processing(self, timg, tdrawable):
        ( ColorFill, ) = self.get_parameters_value()
        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr        

        gimp.image_undo_group_start(timg)
                
        xy_pts =  GetPointsVector( timg, 2 )
        if xy_pts is  None :
            return ( "EXECUTION_ERROR",_("Aborted : bad vector - expected 2 points"))
                
        gimp.item_transform_translate(tdrawable, xy_pts[1][0]- xy_pts[0][0], xy_pts[1][1]- xy_pts[0][1])
        
        color=self.GetColor(tdrawable, ColorFill)
        print("* color=",color)    
        if color is not None :     
            color_save = gimp.context_get_background()[1]
            gimp.context_set_background( color )
            gimp.layer_resize_to_image_size(tdrawable)
            gimp.context_set_background( color_save )
        
        gimp.image_undo_group_end(timg)
        gimp.displays_flush()
        return ( "SUCCESS","")

app=CMoveto()
app.CreerPluginScm()
app.register()
