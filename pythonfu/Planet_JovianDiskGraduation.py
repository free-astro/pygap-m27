#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
import os
import math

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,bullet,menu_planet,init_language
from pyGapM27 import info_right0 as info_right
init_language()

import gp2_func as gimp
from gp2_plugin    import CPlugin
from libTools      import check_compatibilty
from libDisk       import CDiskGraduation

class CJovianDiskGraduation( CPlugin):
    def __init__(self):
        shelp = (  _("Graduate the jovian disk") + ":"
              , bullet + _("In first, set a path with three points on the perimeter")
              , bullet + _("And next, launch the plugin") )
        params = [
            self.Param("COLOR" , "color"     , _("Line color"), 0),
            self.Param("INT32" , "width"     , _("Line width"), 0),
            self.Param("STRING", "font"      , _("Font")      ,""),
            self.Param("COLOR" , "font_color", _("Font color"), 0),
            self.Param("INT32" , "font_size" , _("Font size") , 0)]

        gui = {
            "color"      : self.Box("SF-COLOR"     , '(list 255 255 255 )'      ) ,
            "width"      : self.Box("SF-ADJUSTMENT", '(list 1 1 100  1 1 0 0 )' ) ,
            "font"       : self.Box("SF-FONT"      , '"Courier"'                ) ,
            "font_color" : self.Box("SF-COLOR"     , '(list 255 255 255 )'      ) ,
            "font_size"  : self.Box("SF-ADJUSTMENT", '(list 12 5 100 1 1 0 1 )' ) }

        CPlugin.__init__( self,
            Pkg+"JovianDiskGraduation"   , # name
            _("Graduate the jovian disk"), # blurb
            shelp                        , # help
            info_right                   , # author,copyright, ...
            _("Graduate the jovian disk"), # menu_label
            menu_planet                  , # menu path
            "RGB*, GRAY*"                , # image_types
            params                       , # params
            gui, dir_scriptfu            ) # gui

    def Processing(self, timg, tdrawable):
        ( color, width, font, font_color, font_size) = self.get_parameters_value()

        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr

        Rs=69911 # Rayon solaire en km
        perimetre = 2*math.pi*Rs
        nb_graduation = int( perimetre / 24000 )
        app=CDiskGraduation( timg, tdrawable, color, width, Rs ,
                             nb_graduation, False,0,_("Jupiter"),
                             font, font_color, font_size )
        app.Processing()

        return ( "SUCCESS","")

app=CJovianDiskGraduation()
app.CreerPluginScm()
app.register()
