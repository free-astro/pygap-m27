#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,bullet,menu_moon,init_language
from pyGapM27 import info_right0 as info_right
init_language()

import gp2_func as gimp
from gp2_plugin    import CPlugin
from libTools      import check_compatibilty

class CTargetDrawing( CPlugin):
    def __init__(self):
        shelp = ( _("Draw a target on the moon image")
              , bullet + _("In first, select a area")
              , bullet + _("And next, launch the plugin") )
        params = [
            self.Param("COLOR", "color" , _("Line color") , 0.0),
            self.Param("INT32", "width" , _("Line width") , 0.0),
            self.Param("FLOAT", "ltiret", _("Line length"), 0  )]
        gui = {
            "color"   : self.Box("SF-COLOR"     , '(list 255 255 255 )'              ) ,
            "width"   : self.Box("SF-ADJUSTMENT", '(list 2 1 100  1 1 0 0 )'         ) ,
            "ltiret"  : self.Box("SF-ADJUSTMENT", '(list 0.15 0.1 0.4 0.05 0.1 2 0 )') }

        CPlugin.__init__( self,
            Pkg+"TargetDrawing", # name
            _("Draw a target") , # blurb
            shelp              , # help
            info_right         , # author,copyright, ...
            _("Draw a target") , # menu_label
            menu_moon          , # menu path
            "RGB*, GRAY*"      , # image_types
            params             , # params
            gui, dir_scriptfu  ) # gui

    def Processing(self, timg, tdrawable):
        (color, width, ltiret) = self.get_parameters_value()

        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr

        ss = gimp.selection_bounds(timg)[1]
        if ss[0] == 0 :
            gimp.message( _("Select a area")
                      + "\n" + _("and next, launch again the function") )
            return ( "SUCCESS","")

        gimp.image_undo_group_start(timg)

        gimp.selection_none(timg)

        if gimp.drawable_is_rgb(tdrawable):
            image_type= "RGBA_IMAGE"
        else:
            image_type= "GRAYA_IMAGE"

        drw_width  = gimp.drawable_width(  tdrawable)
        drw_height = gimp.drawable_height( tdrawable)
        layer_new = gimp.layer_new( timg, "TARGET", drw_width, drw_height,
                                image_type , 100.0, "NORMAL")
        gimp.image_insert_layer( timg, layer_new, None,-1)
        gimp.image_set_active_layer( timg, layer_new)
        gimp.layer_add_alpha(layer_new)
        gimp.context_set_foreground( color )

        ww=ss[3]-ss[1]
        hh=ss[4]-ss[2]
        gimp.image_select_ellipse(timg,"REPLACE",ss[1],ss[2],ww,hh)

        gimp.drawable_edit_fill( layer_new, "FOREGROUND_FILL")

        gimp.image_select_ellipse(timg,"REPLACE", ss[1]+width,ss[2]+width, ww-2*width,hh-2*width)
        gimp.edit_cut(layer_new)
        gimp.selection_none(timg)

        pourcent=ltiret
        xx=int(ss[1]+ww/2-width/2)
        yy=int(ss[2]+width-hh*pourcent)
        gimp.image_select_rectangle(timg,"REPLACE", xx,yy,width,int(hh*pourcent))
        gimp.drawable_edit_fill( layer_new, "FOREGROUND_FILL")
        gimp.selection_none(timg)

        xx=int(ss[1]+ww/2-width/2)
        yy=int(ss[2]+hh-width)
        gimp.image_select_rectangle(timg,"REPLACE", xx,yy,width,int(hh*pourcent))
        gimp.drawable_edit_fill( layer_new, "FOREGROUND_FILL")
        gimp.selection_none(timg)

        xx=int(ss[1]+width-int(ww*pourcent))
        yy=int(ss[2]+hh/2-width/2)
        gimp.image_select_rectangle(timg,"REPLACE", xx,yy,int(ww*pourcent),width)
        gimp.drawable_edit_fill( layer_new, "FOREGROUND_FILL")
        gimp.selection_none(timg)

        xx=int(ss[1]+ww-width/2)
        yy=int(ss[2]+hh/2-width/2)
        gimp.image_select_rectangle(timg,"REPLACE",xx,yy,int(ww*pourcent),width)
        gimp.drawable_edit_fill( layer_new, "FOREGROUND_FILL")
        gimp.selection_none(timg)

        gimp.image_undo_group_end(timg)
        gimp.displays_flush()

        return ( "SUCCESS","")

app=CTargetDrawing()
app.CreerPluginScm()
app.register()
