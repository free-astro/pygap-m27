#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
#
#   Python_Merge_Channels - merges separated colour channels in an image
#   Copyright (C) 2015 Bill Smith
#
import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,menu_imagetools,init_language
from pyGapM27 import info_right1 as info_right
init_language()

import gp2_func  as gimp
from gp2_plugin    import CPlugin
from libTools      import check_compatibilty

class CMergeChannels( CPlugin):    
    def __init__(self):
        shelp  = _("Merge separated colour channels by addition") 
        params = []
        gui    = {}

        CPlugin.__init__( self,
            Pkg+"MergeChannels"            , # name
            _("Merge colour channels by addition") , # blurb
            shelp                          , # help
            info_right                     , # author,copyright, ...
            _("Merge colour channels")     , # menu_label
            menu_imagetools+"/"+_("Color") , # menu path
            "RGB*"                         , # image_types
            params                         , # params
            gui, dir_scriptfu              ) # gui

    def Processing(self, timg, tdrawable):
        ''' Merges separated colour channels in image.

        Parameters:
        timg     : The currently selected image.
        tdrawable: The layer of the currently selected image.
        '''
        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr        

        layers = gimp.get_layers( timg ) 
        gimp.image_undo_group_start(timg)
        if len(layers) == 3 :
            gimp.layer_set_mode(layers[0], "ADDITION")
            gimp.layer_set_mode(layers[1], "ADDITION")
            gimp.layer_set_mode(layers[2], "NORMAL"  )
            gimp.image_flatten(timg)
            layers = gimp.get_layers( timg ) 
            gimp.item_set_name(layers[0],"RGB image")
        else:
            gimp.message( _("Required 3 layers ( R,G & B ) for merging") )
        gimp.image_undo_group_end(timg)
        gimp.progress_end()

        return ( "SUCCESS", "")

app=CMergeChannels()
app.CreerPluginScm()
app.register()
