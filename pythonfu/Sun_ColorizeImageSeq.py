#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,bullet,tab,menu_sun,init_language
from pyGapM27 import info_right0 as info_right
init_language()

import gp2_func as gimp
from gp2_plugin    import CPlugin
from libTools      import check_compatibilty,colorize_image
from plug_in       import unsharp_mask,png_save

class CColorizeSunSeq( CPlugin):
    def __init__(self):
        shelp = ( _("Colorize monochrome solar images:")
              , bullet + _("Set the color: user defined/white light/K-Line/Halpha")
              , bullet + _("Set the level of each layer (RGB) for user defined")
              , bullet + _("Set the blur level of the color layer")
              , bullet + _("Enable sharpening")
              , tab + bullet + _("Set the sharpen radius")
              , tab + bullet + _("Set the sharpen amount")
              , tab + bullet + _("Set the sharpen threshold")
              , bullet + _("Select current image or all images")
              , bullet + _("Set image prefix to save") )
        
        params = [
               self.Param("INT32" , "ColorMode"       , _('Color Mode')         , 0  ),
                                                                                
               self.Param("FLOAT" , 'ValueLevel'      , _("Value level")        , 1.00),
               self.Param("FLOAT" , 'RedLevel'        , _("Red level")          , 1.30),
               self.Param("FLOAT" , 'GreenLevel'      , _("Green level")        , 0.80),
               self.Param("FLOAT" , 'BlueLevel'       , _("Blue level")         , 0.20),
               self.Param("FLOAT" , 'Blur'            , _("Blur level")         , 2.50),
                                                                                
               self.Param("INT32" , 'SharpenEnable'   , _("Sharpen Enable")     , 0  ),
               self.Param("FLOAT" , 'SharpenRadius'   , _("sharpen radius")     , 3.0),
               self.Param("FLOAT" , 'SharpenAmount'   , _("sharpen amount")     , 0.1),
               self.Param("FLOAT" , 'SharpenThreshold', _("sharpen threshold")  , 0  ),

               self.Param("INT32" , 'ApplyAll'        , _("Apply on all images"),  0),
               self.Param("STRING", 'PrefixSave'      , _("Image prefix (save)"), "")]
        
        gui = {
            "ColorMode"       : self.Box( "SF-OPTION"    , '(list "' + _("user defined") +'" "white light"  "K-Line" "Continuum" "Halpha (soft)" "Halpha (hard)" )'),
            'ValueLevel'      : self.Box( "SF-ADJUSTMENT", '(list %f 0.1 2.15  0.01 0.1 2 0 )' ) ,
            'RedLevel'        : self.Box( "SF-ADJUSTMENT", '(list %f 0.0 2.15  0.01 0.1 2 0 )' ) ,
            'GreenLevel'      : self.Box( "SF-ADJUSTMENT", '(list %f 0.0 2.15  0.01 0.1 2 0 )' ) ,
            'BlueLevel'       : self.Box( "SF-ADJUSTMENT", '(list %f 0.0 2.15  0.01 0.1 2 0 )' ) ,
            'Blur'            : self.Box( "SF-ADJUSTMENT", '(list %f 0.0 10.0  0.01 0.1 2 0 )' ) ,

            'SharpenEnable'   : self.Box( "SF-TOGGLE"    , 'FALSE'                             ) ,
            'SharpenRadius'   : self.Box( "SF-ADJUSTMENT", '(list %f 0 9  0.01 0.1 3 0 )'      ) ,
            'SharpenAmount'   : self.Box( "SF-ADJUSTMENT", '(list %f 0 5  0.01 0.1 3 0 )'    ) ,
            'SharpenThreshold': self.Box( "SF-ADJUSTMENT", '(list %d 0 255  1 10 0 0 )'         ) ,

            'ApplyAll'        : self.Box( "SF-TOGGLE"    , 'FALSE'                             ) ,
            'PrefixSave'      : self.Box( "SF-STRING"    , '"COLOR-"'                          )
            }

        CPlugin.__init__( self,
            Pkg+"ColorizeSunSeq"         , # name
            _("Colorize Sun (Sequence)") , # blurb
            shelp                        , # help
            info_right                   , # author,copyright, ...
            _("Colorize Sun (Sequence)") , # menu_label
            menu_sun +"/"+ _("Colorize") , # menu path
            "RGB*, GRAY*"                , # image_types
            params                       , # params
            gui, dir_scriptfu            ) # gui

    def Processing(self, timg, tdrawable):
        (   ColorMode, ValueLevel, RedLevel, GreenLevel, BlueLevel,  Blur,
            SharpenEnable, SharpenRadius, SharpenAmount, SharpenThreshold,
            ApplyAllImages, PrefixSave ) = self.get_parameters_value()
            
        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr        

        if ColorMode == 1  : # white light
            ( ValueLevel, RedLevel, GreenLevel, BlueLevel)=(1.0,1.3,0.8,0.2)
        if ColorMode == 2  : # K-Line
            ( ValueLevel, RedLevel, GreenLevel, BlueLevel)=(1.0,0.7,0.8,1.2)
        if ColorMode == 3  : # Continuum
            ( ValueLevel, RedLevel, GreenLevel, BlueLevel)=(1.0,0.12,0.9,0.18)
        if ColorMode == 4  : # Halpha light
            ( ValueLevel, RedLevel, GreenLevel, BlueLevel)=(1.0,1.3,0.6,0.2)
        if ColorMode == 5  : # Hard
            ( ValueLevel, RedLevel, GreenLevel, BlueLevel)=(1.0,1.0,0.0,0.0)

        if ApplyAllImages :
            for i_img in gimp.image_list() :
                i_drawable = gimp.image_get_active_drawable(i_img)
                self.Processing1image(i_img, i_drawable,
                             ValueLevel, RedLevel, GreenLevel, BlueLevel,  Blur,
                             SharpenEnable, SharpenRadius, SharpenAmount, SharpenThreshold,
                             PrefixSave )
                gimp.displays_flush()
        else:
            self.Processing1image(timg, tdrawable,
                             ValueLevel, RedLevel, GreenLevel, BlueLevel,  Blur,
                             SharpenEnable, SharpenRadius, SharpenAmount, SharpenThreshold,
                             PrefixSave )
            gimp.displays_flush()
        
        self.CreerPluginScm(force=True)

        return ( "SUCCESS","")

    def Processing1image(self, i_img, i_drawable, ValueLevel, RedLevel, GreenLevel, BlueLevel,  Blur,
                SharpenEnable, SharpenRadius, SharpenAmount, SharpenThreshold,
                PrefixSave ):
        _unused=(i_drawable) # unused parameter , suppress pylint warning

        gimp.image_undo_group_start(i_img)   
        
        LayerRGB,layer_L = colorize_image(i_img, i_drawable, ValueLevel, RedLevel, GreenLevel, BlueLevel, Blur)
     
        if SharpenEnable :
            unsharp_mask( i_img, layer_L, SharpenRadius, SharpenAmount, SharpenThreshold )

        infilename = gimp.image_get_filename( i_img )
        infilename = os.path.splitext(infilename)[0]

        outfilename = os.path.join( os.path.dirname(infilename) , PrefixSave + os.path.basename(infilename) + b".png" )
        png_save(i_img, LayerRGB, outfilename)
        gimp.image_set_filename( i_img, outfilename )
        #self.file_export( i_img, outfilename )

        gimp.image_undo_group_end(i_img)
        return ( "SUCCESS","")

    """
    # alternative a plug_in_png_save ( quelque soit l'extension )
    def file_export(self,  timg, filename ) :
        img_cpy = gimp.image_duplicate(timg)
        layer_flatten =gimp.image_flatten(img_cpy)

        # copie d'un calque dans l'image d'origine
        layer_cpy = gimp.layer_new_from_drawable(layer_flatten,timg)
        gimp.image_delete(img_cpy)

        # insertion du calque a la position courante (hors group)
        gimp.image_insert_layer(timg,layer_cpy, None,-1)
        gimp.image_set_active_layer( timg, layer_cpy)
        gimp.item_set_name(layer_cpy,"flatten layer")

        gimp.file_save(1, timg, layer_cpy, filename , '?')
        gimp.image_remove_layer(timg,layer_cpy)
    """
    
app=CColorizeSunSeq()
app.CreerPluginScm()
app.register()
