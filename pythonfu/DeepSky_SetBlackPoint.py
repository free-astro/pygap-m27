#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
#
#   Python-Set-Black-Point - Set the image black point automatically.
#   Copyright (C) 2017  Bill Smith
#
import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,menu_background,init_language
from pyGapM27 import info_right1 as info_right
init_language()

import gp2_func  as gimp
from gp2_plugin    import CPlugin
from libTools      import check_compatibilty
from libTools      import set_black_point

class CSetBlackPoint( CPlugin):    
    def __init__(self):
        shelp  = ( _("set the image black point automatically") + ":"
               , _("Adjusts the deviation (in Std.Dev) from the signal average to set the black level ") )
        params = [self.Param("FLOAT", "ksigma", _('sigma factor'), 0.0)]
        gui    = {"ksigma" : self.Box("SF-ADJUSTMENT", '(list 1.0 0.1 3.00  0.1 1.0 2 0 )')}

        CPlugin.__init__( self,
            Pkg+"SetBlackPoint"            , # name
            _("Set the image black point") , # blurb
            shelp                          , # help
            info_right                     , # author,copyright, ...
            _("Set the image black point") , # menu_label
            menu_background                , # menu path
            "RGB*"                         , # image_types
            params                         , # params
            gui, dir_scriptfu              ) # gui

    def Processing(self, timg, tdrawable):
        ''' This GIMP plugin sets the image black point automatically.

        Parameters:
        timg     : The currently selected image.
        tdrawable: The layer of the currently selected image.
        ksigma   : Set the deviation factor (in Std.Dev )
        '''
        (img, layer) = (timg, tdrawable)
        (ksigma,) = self.get_parameters_value()

        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr
            
        set_black_point(img, layer, ksigma)

        return ( "SUCCESS", "")

app=CSetBlackPoint()
app.CreerPluginScm()
app.register()
