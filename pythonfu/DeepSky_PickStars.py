#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
#
#   Python-Pick-Stars - Picks out the stars in an image.
#   Copyright (C) 2015  Bill Smith
#
import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,menu_stars,init_language
from pyGapM27 import info_right1 as info_right
init_language()

import gp2_func  as gimp
from gp2_plugin    import CPlugin
from libTools      import check_compatibilty
from libTools      import create_layer,fill_layer,getMaxDyn

class CPickStars( CPlugin):    
    def __init__(self):
        shelp  = _("pick out the stars above a max percentage brightness") 
        params = [
            self.Param("FLOAT", "r_pick", _('Red limit')  , 0.0),
            self.Param("FLOAT", "g_pick", _('Green limit'), 0.0),
            self.Param("FLOAT", "b_pick", _('Blue limit') , 0.0)]
        gui = {
            "r_pick" : self.Box("SF-ADJUSTMENT", '(list 90.0 0.0 100.0 1.0 10.0 1 0 )'),
            "g_pick" : self.Box("SF-ADJUSTMENT", '(list 90.0 0.0 100.0 1.0 10.0 1 0 )'),
            "b_pick" : self.Box("SF-ADJUSTMENT", '(list 90.0 0.0 100.0 1.0 10.0 1 0 )')}

        CPlugin.__init__( self,
            Pkg+"PickStars"            , # name
            _("pick out the stars in an image") , # blurb
            shelp                      , # help
            info_right                 , # author,copyright, ...
            _("Pick out stars")        , # menu_label
            menu_stars                 , # menu path
            "RGB*"                     , # image_types
            params                     , # params
            gui, dir_scriptfu          ) # gui

    def Processing(self, timg, tdrawable):
        ''' This GIMP plugin picks out stars in an image.

        Parameters:
        timg     : The currently selected image.
        tdrawable: The layer of the currently selected image.
        r_pick   : pick limit on colour red
        g_pick   : pick limit on colour green
        b_pick   : pick limit on colour blue
        '''
        (img, layer) = (timg, tdrawable)
        (r_pick, g_pick, b_pick) = self.get_parameters_value()

        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr

        nChan=3
        if gimp.drawable_has_alpha(layer): nChan+=1
        MaxDyn = getMaxDyn(img)

        factor       = MaxDyn/100.0
        rgba_min     = [ r_pick * factor, g_pick * factor, b_pick * factor, 0 ]        
        
        gimp.context_push()
        gimp.image_undo_group_start(img)
                
        mask = create_layer(img, layer,"Mask ...", "LIGHTEN_ONLY", -1 )
        fill_layer(mask,rgba_min)
        layer_merge = gimp.image_merge_down(img, mask, "EXPAND_AS_NECESSARY" )        

        layer_threshold = gimp.layer_copy(layer_merge, False)
        gimp.image_insert_layer(img, layer_threshold, 0, -1)
        delta=0.004 # ~1/256
        gimp.drawable_threshold(layer_threshold,'RED'  ,rgba_min[0]+delta,1.0)
        gimp.drawable_threshold(layer_threshold,'GREEN',rgba_min[1]+delta,1.0)
        gimp.drawable_threshold(layer_threshold,'BLUE' ,rgba_min[2]+delta,1.0)
        
        gimp.layer_set_mode(layer_threshold, "MULTIPLY")
        layer_merge = gimp.image_merge_down(img, layer_threshold, "EXPAND_AS_NECESSARY" )        
        
        gimp.image_undo_group_end(img)       
       
        gimp.context_pop()
        gimp.displays_flush()

        return ( "SUCCESS", "")

app=CPickStars()
app.CreerPluginScm()
app.register()
