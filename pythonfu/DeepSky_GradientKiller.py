#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
import os
import time

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,bullet,tab,menu_noise,init_language
from pyGapM27 import info_right0 as info_right
init_language()

from libGridSelection import CGridSelection

import gp2_func   as gimp
from gp2_plugin import CPlugin
from plug_in    import gauss_iir
from gp2_tools  import GetRegionRect,UpdateRegionRect
from libTools   import check_compatibilty
from libTools   import GetInfoImg,copy_layer,copy_layer_to_image

class CGradientKiller( CPlugin):
    def __init__(self):
        shelp = (_("Calculate the gradient and substract it to the image")
              , _("Note that the plugin is launched in 2 times:")
              , bullet + _("At the first launch, a selection grid is generated")
              , bullet + _("Then, the user deselects the zones containing")
              , bullet + _("the object (nebula, galaxy, bright stars, ...)")
              , bullet + _("On the second launch, the gradient is generated")
              , tab    + _("and subtracted from the image") )
        params = [
            self.Param("INT32", 'rayon'    , _("Size")         , 0),
            self.Param("INT32", 'nb_xpoint', _("Point by line"), 0)]
        gui = {
            'rayon'    : self.Box("SF-ADJUSTMENT", '(list 200 20 400  1 10 0 0 )' ) ,
            'nb_xpoint': self.Box("SF-ADJUSTMENT", '(list  20 11 100  1  1 0 0 )' ) }

        CPlugin.__init__( self,
            Pkg+"GradientKiller"      , # name
            _("Gradient suppression") , # blurb
            shelp                     , # help
            info_right                , # author,copyright, ...
            _("Gradient suppression") , # menu_label
            menu_noise                , # menu path
            "RGB*, GRAY*"             , # image_types
            params                    , # params
            gui, dir_scriptfu         ) # gui

    def Processing(self, timg, tdrawable):
        (rayon, nb_xpoint) = self.get_parameters_value()

        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr

        self.precision= gimp.image_get_precision(timg)

        gimp.image_undo_group_start(timg)

        nb_xpoint = int(nb_xpoint)
        rayon     = int(rayon)

        ss = gimp.selection_bounds(timg)[1]
        if ss[0] == 0 :
            # pass 1 : selection
            grid = CGridSelection( timg, tdrawable, nb_xpoint, rayon)
            grid.set( )
            gimp.message( _("Deselects the zones containing the object (nebula, galaxy, bright stars, ... )")
                      + "\n" + _("and next, launch again the function") )
            time.sleep(1)
        else:
            # pass 2 : Calcul le gradient
            self.process(timg, tdrawable, nb_xpoint, rayon)

        gimp.image_undo_group_end(timg)
        gimp.displays_flush()
        return ( "SUCCESS","")

    def get_grid( self, grid , rvb_default):
        xy_list=[]
        rayon=grid.get_rayon_grid()
        for xy in grid.xy_grid( ) :
            s=gimp.selection_value(grid.get_img(), int(xy[0]+rayon/2), int(xy[1]+rayon/2) )
            xy_list.append( { 'xy' : (xy[0], xy[1]), 'rayon':rayon,  'ON':s != 0, 'color':[cc for cc in rvb_default] } )
        return  xy_list

    def get_color(self, timg, tdrawable ):
        _unused=(timg) # unused parameter , suppress pylint warning
        if gimp.drawable_is_rgb(tdrawable) :
            red   = gimp.drawable_histogram(tdrawable, "RED"  ,0.0,1.0)[1][2]
            green = gimp.drawable_histogram(tdrawable, "GREEN",0.0,1.0)[1][2]
            blue  = gimp.drawable_histogram(tdrawable, "BLUE" ,0.0,1.0)[1][2]
            return [ red, green ,blue ]
        else:
            median = gimp.drawable_histogram(tdrawable, "VALUE"  ,0.0,1.0)[1][2]
            return [ median ]

    def BuildGradient_Method_direct(self, timg, tdrawable, xy_color, nb_xpoint, nb_ypoint,width,height,rayon_blur):
        img_type  = gimp.image_base_type(timg)
        sPrecision= GetInfoImg(self.precision)[0]
        img_grad  = gimp.image_new_with_precision( nb_xpoint, nb_ypoint, img_type, sPrecision )
        #img_disp   = gimp.display_new(img_grad)
        layer_grad = gimp.layer_new(img_grad, "gradient",nb_xpoint, nb_ypoint,
                                    gimp.drawable_type(tdrawable), 100, "NORMAL")
        gimp.image_insert_layer(img_grad,layer_grad, None,-1)
        layer_grad = gimp.image_flatten(img_grad) # astuce pour supprimer une couche Alpha

        ii=0
        pp=0
        CharFmt = GetInfoImg(self.precision)[2]
        MaxDyn  = GetInfoImg(self.precision)[4]
        flag_int = True
        if GetInfoImg(self.precision)[1]== 8 : MaxDyn=1.0
        if GetInfoImg(self.precision)[1]== -32 :
            MaxDyn=1.0
            flag_int=False

        (sPixelRgn, srcRgn, src_pixels, ) = GetRegionRect(layer_grad, CharFmt, 0, 0, nb_xpoint, nb_ypoint)

        for ll in range(nb_ypoint):
            for cc in range(nb_xpoint):
                xy=xy_color[ii]
                for chan in xy['color'] :
                    pixel= chan*MaxDyn
                    if flag_int :
                        pixel = int(pixel)
                    src_pixels[pp] = pixel
                    pp+=1
                ii+=1

        UpdateRegionRect(layer_grad, sPixelRgn, srcRgn, src_pixels)

        gimp.image_scale( img_grad, width, height )
        for ii in range(3):
            gauss_iir(img_grad, layer_grad, rayon_blur, True, True )
        return [ img_grad, layer_grad ]

    def process(self, timg, tdrawable, nb_xpoint, rayon):
        tdrawable_cpy = copy_layer(timg, tdrawable, None, -1 )
        # fort lissage pour separer les modes dans l'histo
        for ii in range(3):
            gauss_iir(timg, tdrawable_cpy, rayon, True, True )

        grid        = CGridSelection( timg, tdrawable_cpy, nb_xpoint, rayon)
        rayon       = grid.get_rayon_grid( )
        rvb_default = self.get_color( timg, tdrawable_cpy )
        xy_color    = self.get_grid( grid, rvb_default )

        gimp.selection_none(timg)
        for xy in xy_color :
            if  xy['ON'] == True :
                gimp.image_select_rectangle( timg, "REPLACE", xy['xy'][0],  xy['xy'][1],rayon,rayon)
                xy['color'] = self.get_color( timg, tdrawable_cpy  )
        gimp.selection_none(timg)
        gimp.image_remove_layer(timg, tdrawable_cpy)

        val_min=1.0
        val_max=0.0
        for xy in xy_color :
            val_min = min( val_min,min(xy['color']))
            val_max = max( val_max,max(xy['color']))

        ratio=0.95
        for xy in xy_color :
            for ii in range(len(xy['color'] ) ) :
                xy['color'][ii] = (xy['color'][ii] - val_min)*ratio

        nb_ypoint = grid.get_ypoints_grid( )
        w=gimp.image_width(timg)
        h=gimp.image_height(timg)
        (img_grad, layer_grad)=self.BuildGradient_Method_direct( timg, tdrawable, xy_color, nb_xpoint, nb_ypoint, w, h,rayon)

        # copie d'un calque dans une autre imge
        layer_cpy = copy_layer_to_image(img_grad,layer_grad,timg,_("gradient layer"), -1 )
        gimp.layer_set_mode(layer_cpy,"SUBTRACT" )
        gimp.layer_set_opacity(layer_cpy,20)

app=CGradientKiller()
app.CreerPluginScm()
app.register()
