#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,menu_layertools,init_language
from pyGapM27 import info_right0 as info_right
init_language()

import gp2_func as gimp
from gp2_plugin    import CPlugin
from libTools      import check_compatibilty,copy_layer

class CMaskLayer( CPlugin):
    def __init__(self):
        shelp  = _("Creating an object mask layer")
        params = [ self.Param("INT32", 'ObjLayer', _("Mask Layer"), 0 )]
        gui    = { 'ObjLayer': self.Box("SF-DRAWABLE", '0' ) }

        CPlugin.__init__( self,
            Pkg+"MaskLayer"   , # name
            _("Layer mask")   , # blurb
            shelp             , # help
            info_right        , # author,copyright, ...
            _("Layer mask")   , # menu_label
            menu_layertools   , # menu path
            "RGB*, GRAY*"     , # image_types
            params            , # params
            gui, dir_scriptfu ) # gui

    def Processing(self, timg, tdrawable):
        (ObjLayer,) = self.get_parameters_value()

        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr        

        gimp.image_undo_group_start(timg)

        layer_cpy1 = copy_layer(timg,tdrawable,_("Object Mask Layer"), -1,True)
        mask_layer = gimp.layer_create_mask(layer_cpy1, "BLACK")
        gimp.layer_add_mask(layer_cpy1,mask_layer)

        gimp.image_set_active_layer( timg, ObjLayer )
        gimp.selection_all(timg)
        gimp.edit_copy(ObjLayer)
        gimp.image_set_active_layer( timg, ObjLayer )

        float_sel = gimp.edit_paste(mask_layer,True)
        gimp.floating_sel_anchor(float_sel)
        gimp.layer_set_edit_mask( layer_cpy1, False )
        gimp.selection_none( timg )

        gimp.image_undo_group_end(timg)
        gimp.displays_flush()

        return ( "SUCCESS","")

app=CMaskLayer()
app.CreerPluginScm()
app.register()
