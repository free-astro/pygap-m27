#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
#
#   Python-Enhance-Dynamic-Range - Improve contrast in deep sky images
#   Copyright (C) 2017  Bill Smith
#
import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,menu_enhancement,init_language
from pyGapM27 import info_right1 as info_right
init_language()

import gp2_func  as gimp
from gp2_plugin import CPlugin
from libTools   import set_black_point,scale_image_brightness
from libTools   import check_compatibilty
from libTools   import copy_layer,add_mask

class CEnhanceDynamicRange( CPlugin):    
    def __init__(self):
        shelp  = _("Enhance the dynamic range in a deep sky image using Lodriguss method") 
        params = [
            self.Param("FLOAT", "bright", _('Brightness factor')       , 0.0),
            self.Param("FLOAT", "gwdth" , _('Gaussian blur (pixels)')  , 0.0),
            self.Param("FLOAT", "setblk", _('Set black point at start'), 0.0)]
        gui = {
            "bright": self.Box("SF-ADJUSTMENT", '(list  1.2 1.0 10.00  0.1 1.0 1 0 )'),
            "gwdth" : self.Box("SF-ADJUSTMENT", '(list 25.0 1.0 75.00  0.1 1.0 1 0 )'),
            "setblk": self.Box("SF-TOGGLE"    , 'FALSE')}

        CPlugin.__init__( self,
            Pkg+"EnhanceDynamicRange"       , # name
            _("Enhance the dynamic range in a deep sky image") , # blurb
            shelp                           , # help
            info_right                      , # author,copyright, ...
            _("Enhance dynamic range")      , # menu_label
            menu_enhancement                , # menu path
            "RGB*"                          , # image_types
            params                          , # params
            gui, dir_scriptfu               ) # gui

    def Processing(self, timg, tdrawable):
        ''' This GIMP plugin enhances dynamic range in a deep sky image (Lodriguss method).

        Parameters:
        timg     : The currently selected image.
        tdrawable: The layer of the currently selected image.
        bright : The brightness factor (1.0 to 10.0)
        gwdth  : Gaussian blur of mask (1 to 75)
        setblk : Set black point at start (optional)
        '''
        (img, layer) = (timg, tdrawable)
        (bright, gwdth, setblk) = self.get_parameters_value()

        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr

        layers = gimp.get_layers(img)
        num = len(layers)
        gimp.context_push()
        gimp.image_undo_group_start(img)
        if num == 1:
            if setblk:
                layer = set_black_point(img,layers[0],1.0)
                layers = gimp.get_layers(img)
            new_layer_1 = copy_layer(img,layers[0],_("Enhance Dynamic Range Layer1"), None)
            new_layer_2 = copy_layer(img,layers[0],_("Enhance Dynamic Range Layer2"), None)
            if bright > 1: scale_image_brightness(img,layer,bright)  
            gimp.image_insert_layer(img,new_layer_1, None, -1 )
            add_mask(img, new_layer_1, new_layer_2, gwdth )     
        elif num == 2:
            if setblk:
                set_black_point(img,layers[0],1.0)
                set_black_point(img,layers[1],1.0)
            layers = gimp.get_layers(img)
            if bright > 1: scale_image_brightness(img,layers[1],bright)
            layers = gimp.get_layers(img)
            new_layer = gimp.layer_copy(layers[0], False)            
            add_mask(img, layers[0], new_layer, gwdth )   
        gimp.image_undo_group_end(img)
        gimp.context_pop()
    
        return ( "SUCCESS", "")        
        
app=CEnhanceDynamicRange()
app.CreerPluginScm()
app.register()
