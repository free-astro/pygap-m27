#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
#
#   Python-Enhance-Using SVD layers - Boosts the detail in star images.
#   Copyright (C) 2016  Bill Smith
#
import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,menu_enhancement,init_language
from pyGapM27 import info_right1 as info_right
init_language()

import gp2_func  as gimp
from gp2_plugin import CPlugin
from libTools   import check_compatibilty
from libTools   import copy_layer

class CEnhanceUsingSVD( CPlugin):    
    def __init__(self):
        shelp  = _("Enhance using Soft light-, Value- and Dodge-mode layers") 
        params = [self.Param("FLOAT" , "opac" , _('Dodge layer opacity'), 0.0)]
        gui = {"opac" : self.Box("SF-ADJUSTMENT", '(list 50.0 0.01 100.00  0.1 1.0 2 0 )')}

        CPlugin.__init__( self,
            Pkg+"EnhanceUsingSVD"         , # name
            _("Enhance using SVD layers") , # blurb
            shelp                         , # help
            info_right                    , # author,copyright, ...
            _("Enhance using SVD layers") , # menu_label
            menu_enhancement              , # menu path
            "RGB*"                        , # image_types
            params                        , # params
            gui, dir_scriptfu             ) # gui

    def Processing(self, timg, tdrawable):
        ''' This GIMP plugin enhances an image using Soft light, Value
        (luminance) and Dodge layers (SVD).

        timg     : The currently selected image.
        tdrawable: The layer of the currently selected image.
        opac      : Opacity of Dodge layer.
        '''
        (img, layer) = (timg, tdrawable)
        (opacity,) = self.get_parameters_value()

        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr

        mode_merge = "EXPAND_AS_NECESSARY"
        
        gimp.context_push()
        gimp.image_undo_group_start(img)
        
        new_layer_0 = copy_layer(img, layer,"new_layer_0",-1)
        new_layer_1 = copy_layer(img, layer,"new_layer_1",-1)
        new_layer_2 = copy_layer(img, layer,"new_layer_2",-1)

        gimp.layer_set_mode(new_layer_0, "SOFTLIGHT")
        gimp.layer_set_mode(new_layer_1, "HSV_VALUE")
        gimp.layer_set_mode(new_layer_2, "DODGE"    )
        gimp.layer_set_opacity(new_layer_2, opacity)
        
        gimp.image_merge_down(img, new_layer_0, mode_merge)
        gimp.image_merge_down(img, new_layer_1, mode_merge)
        gimp.image_merge_down(img, new_layer_2, mode_merge)
        
        gimp.image_undo_group_end(img)
        gimp.context_pop()

        return ( "SUCCESS", "")

app=CEnhanceUsingSVD()
app.CreerPluginScm()
app.register()
