#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
#
#   Python-Clip-Image-Brightness - Clips the the image brightness.
#   Copyright (C) 2015  Bill Smith
#
import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,menu_enhancement,init_language
from pyGapM27 import info_right1 as info_right
init_language()

import gp2_func  as gimp
from gp2_plugin import CPlugin
from libTools   import check_compatibilty,create_layer,fill_layer

class CClipImageBrightness( CPlugin):    
    def __init__(self):
        shelp  = _("Clip the image at a minimum percentage brightness") 
        params = [
            self.Param("FLOAT", "r_clip", _("Red limit")  , 0.0),
            self.Param("FLOAT", "g_clip", _("Green limit"), 0.0),
            self.Param("FLOAT", "b_clip", _("Blue limit") , 0.0)]
        gui = {
            "r_clip": self.Box("SF-ADJUSTMENT", '(list 25.0 0.0 100.00  1.0 10.0 2 0 )'),
            "g_clip": self.Box("SF-ADJUSTMENT", '(list 25.0 0.0 100.00  1.0 10.0 2 0 )'),
            "b_clip": self.Box("SF-ADJUSTMENT", '(list 25.0 0.0 100.00  1.0 10.0 2 0 )')}

        CPlugin.__init__( self,
            Pkg+"ClipImageBrightness"      , # name
            _("Clip the image brightness") , # blurb
            shelp                          , # help
            info_right                     , # author,copyright, ...
            _("Clip the image brightness") , # menu_label
            menu_enhancement               , # menu path
            "RGB*"                         , # image_types
            params                         , # params
            gui, dir_scriptfu              ) # gui

    def Processing(self, timg, tdrawable):
        ''' This GIMP plugin clips the brightness of an image.

        Parameters:
        timg     : The currently selected image.
        tdrawable: The layer of the currently selected image.
        r_clip : Clip limit on colour red
        g_clip : Clip limit on colour green
        b_clip : Clip limit on colour blue
        '''
        (img, layer) = (timg, tdrawable)
        (r_clip, g_clip, b_clip) = self.get_parameters_value()

        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr
            
        bgcolor       = ( r_clip / 100.0, g_clip / 100.0, b_clip / 100.0)
        mode_subtract = "SUBTRACT_LEGACY" # important LEGACY sinon KO
        mode_merge    = "EXPAND_AS_NECESSARY"
        
        gimp.context_push()
        gimp.image_undo_group_start(img)
        
        # 1) copie du calque courant
        new_layer = gimp.layer_copy(layer, False)
        gimp.image_insert_layer(img, new_layer, None, -1)
        
        # 2) création d'un masque
        mask=create_layer(img, layer,"Mask ...", mode_subtract, -1 ) 
        fill_layer(mask,bgcolor)       
        
        # 3) Fusion du masque sur la copie du calque
        layer_merge = gimp.image_merge_down(img, mask, mode_merge)
        
        # 4) changment de mode puis fusion
        gimp.layer_set_mode(layer_merge, mode_subtract)
        gimp.image_merge_down(img, layer_merge, mode_merge)
        
        gimp.image_undo_group_end(img)
        gimp.context_pop()
    
        return ( "SUCCESS", "")

app=CClipImageBrightness()
app.CreerPluginScm()
app.register()
