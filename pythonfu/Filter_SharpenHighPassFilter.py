#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
#
#   Python-High-Pass-Filter - sharpens image using high pass filter.
#   Copyright (C) 2016  Bill Smith
#
import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))
sys.path.append(os.path.join(path_exec,"lib"))
dir_scriptfu = os.path.join(path_exec, "scriptfu")

from pyGapM27 import Pkg,menu_filters,init_language
from pyGapM27 import info_right1 as info_right
init_language()

import gp2_func  as gimp
from gp2_plugin    import CPlugin
from libTools      import check_compatibilty
from libTools      import copy_layer
from plug_in       import gauss_iir

class CHighPassFilter(CPlugin):    
    def __init__(self):
        shelp  = _("Sharpen with high pass filter") 
        params = [self.Param("FLOAT", "blur", _("Gaussian blur"), 0.0),
                  self.Param("INT32", "pack", _("Flatten image"), 0.0)]
        gui = {"blur" : self.Box("SF-ADJUSTMENT", '(list 1.0 1.0 10.0  0.1 1.0 2 0 )'),
               "pack" : self.Box("SF-TOGGLE", 'TRUE')}

        CPlugin.__init__( self,
            Pkg+"HighPassFilter" , # name
            _("High pass filter"), # blurb
            shelp                , # help
            info_right           , # author,copyright, ...
            _("High pass filter"), # menu_label
            menu_filters         , # menu path
            "RGB*"               , # image_types
            params               , # params
            gui, dir_scriptfu    ) # gui

            
    def Processing(self, timg, tdrawable):
        ''' This GIMP plugin : sharpens image using high pass filter

        Parameters:
        timg     : The currently selected image.
        tdrawable: The layer of the currently selected image.
        blur     : Gaussian blurring parameter
        pack     : Flatten image at end Y/N
        '''
        (img, layer) = (timg, tdrawable)
        (blur,pack) = self.get_parameters_value()

        gimp.plugin_enable_precision() # important sinon traitement en bpp=8
        cr=check_compatibilty(self.proc_name,timg,tdrawable)
        if cr != None : return cr

        desaturate_mode= "LIGHTNESS"
        mode_merge     = "EXPAND_AS_NECESSARY"

        gimp.context_push()
        gimp.image_undo_group_start(img)

        new_layer_1 = copy_layer(img, layer ,"new_layer_1", -1 )
        new_layer_2 = copy_layer(img, layer ,"new_layer_2", -1 )
        gimp.drawable_desaturate(new_layer_2, desaturate_mode)
        gauss_iir(img ,new_layer_2, blur, True, True)
        gimp.layer_set_mode(new_layer_2, "GRAIN_EXTRACT")
        gimp.drawable_desaturate(new_layer_1, desaturate_mode)
        final_mask=gimp.image_merge_down(img, new_layer_2, mode_merge)
        gimp.layer_set_mode(final_mask, "OVERLAY")
        if pack: gimp.image_flatten(img)
        
        gimp.image_undo_group_end(img)
        gimp.context_pop()

        return ( "SUCCESS", "")

app=CHighPassFilter()
app.CreerPluginScm()
app.register()

