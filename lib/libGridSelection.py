#! /usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))

import gp2_func as gimp
from libTools import create_layer, fill_layer

# ------------------------------------------------------------------------------
class CGridSelection:
    def __init__(self, timg, tdrawable, nb_xpoint, rayon ):
        self.timg      = timg
        self.tdrawable = tdrawable
        self.param     = self.set_param_grid( nb_xpoint, rayon )

    def set_param_grid(self, nb_xpoint, rayon):
        x0=0
        x1=gimp.drawable_width(self.tdrawable)-rayon
        w=x1-x0
        nb_xpoint=int(nb_xpoint)
        incr_x = round(w / (nb_xpoint-1))
        rayon = min( rayon, int(0.666*incr_x))
        # 2nd pass (rayon a peut-etre changer
        x1=gimp.drawable_width(self.tdrawable)-rayon
        w=x1-x0
        nb_xpoint=int(nb_xpoint)
        incr_x = round(w / (nb_xpoint-1))
        rayon = min( rayon, int(0.666*incr_x))

        y0=0
        y1=gimp.drawable_height(self.tdrawable)-rayon
        h=y1-y0
        nb_ypoint = int(round( h / incr_x))+1
        incr_y = incr_x
        y0 = int(( h - (nb_ypoint-1)*incr_y)/2)

        return { 'origin'   : [int(x0), int(y0)],
                 'incr'     : [int(incr_x), int(incr_y)],
                 'nb_points': [int(nb_xpoint), int(nb_ypoint)] ,
                 "rayon"    : int(rayon) }

    def get_param_grid(self):
        return self.param

    def get_points_grid(self):
        return self.param['nb_points']

    def get_xpoints_grid(self):
        return self.param['nb_points'][0]

    def get_ypoints_grid(self):
        return self.param['nb_points'][1]

    def get_rayon_grid(self):
        return self.param['rayon']

    def get_img(self):
        return self.timg

    def get_drawable(self):
        return self.tdrawable

    def xy_grid(self):
        (x0, y0)               = self.param['origin']
        (incr_x, incr_y)       = self.param['incr']
        (nb_xpoint, nb_ypoint) = self.param['nb_points']
        y=y0
        xy_list=[]
        for ll in range(0,nb_ypoint):
            x=x0
            for cc in range(0,nb_xpoint):
                xy_list.append( (x, y) )
                x = x + incr_x
            y = y + incr_y
        return xy_list

    # creer la grille
    def set(self):
        Noir =(0.0,0.0,0.0,0.0)
        Blanc=(1.0,1.0,1.0,0.0)

        timg   = self.timg
        rayon  = self.param['rayon']
        
        layerg=create_layer(timg, self.tdrawable, "GRID", "NORMAL", -1, 100.0)
        fill_layer( layerg,Noir)
        gimp.context_set_foreground( Blanc )

        for xy in self.xy_grid( ) :
            gimp.image_select_rectangle( timg, "REPLACE", xy[0], xy[1], rayon, rayon)
            gimp.edit_fill(layerg,"FOREGROUND_FILL")

        gimp.edit_fill(layerg,"FOREGROUND_FILL") # dans le cas d'un echec
        gimp.selection_none(timg)

        gimp.image_select_color(timg,"REPLACE",layerg, Blanc )
        gimp.image_remove_layer(timg,layerg)
