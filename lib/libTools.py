#! /usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append('.')
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))

import gp2_func  as gimp
from   gp2_tools import GetInfoImg
from   plug_in   import gauss_iir


def scale_image_brightness(img, layer, bright):
    divide_mode = "DIVIDE"
    mode_merge  = "EXPAND_AS_NECESSARY"
    basec       = 1.0 / bright
    bgcolor     = (basec, basec, basec)

    gimp.context_push()
    gimp.image_undo_group_start(img)

    newlay = create_layer(img, layer, "Mask ...", divide_mode )
    fill_layer(newlay, bgcolor )
    layer_new = gimp.image_merge_down(img, newlay, mode_merge )

    gimp.image_undo_group_end(img)
    gimp.context_pop()

    return layer_new

def set_black_point(img, layer, ksigma):
    nChan=3
    if gimp.drawable_has_alpha(layer): nChan+=1
    MaxDyn = getMaxDyn(img)

    (avg_red,sig_red) = gimp.drawable_histogram(layer,"RED"  ,0.0,1.0)[1][0:2]
    (avg_grn,sig_grn) = gimp.drawable_histogram(layer,"GREEN",0.0,1.0)[1][0:2]
    (avg_blu,sig_blu) = gimp.drawable_histogram(layer,"BLUE" ,0.0,1.0)[1][0:2]

    avg_red *= MaxDyn *1.0
    avg_grn *= MaxDyn *1.0
    avg_blu *= MaxDyn *1.0
    
    sig_red *= MaxDyn *ksigma
    sig_grn *= MaxDyn *ksigma
    sig_blu *= MaxDyn *ksigma

    if avg_red < sig_red and avg_grn < sig_grn and avg_blu < sig_blu:
        msg=_("average: ") + "(%.3f,%.3f,%3f) < k.sigma: (%.3f,%.3f,%3f)\n\n" %(
              avg_red,avg_grn,avg_blu, sig_red,sig_grn,sig_blu)
        gimp.message(msg + _("Black point reset not practical for this image"))
        return layer  # = KO status

    ddrgb= [  max(0.0,avg_red-sig_red/2.0),
              max(0.0,avg_grn-sig_grn/2.0),
              max(0.0,avg_blu-sig_blu/2.0),
              0.0 ]

    gimp.context_push()
    gimp.image_undo_group_start(img)

    mask = create_layer(img, layer, "Mask ...", "SUBTRACT", -1)
    gimp.layer_set_opacity(layer,100.0)    
    fill_layer(mask, ddrgb )
    new_layer = gimp.image_merge_down(img, mask, "EXPAND_AS_NECESSARY" )

    gimp.image_undo_group_end(img)
    gimp.context_pop()
    gimp.displays_flush()

    return new_layer  # = OK status

def create_img(img, layer):
    """ Create a empty image with the same properties as ( img,layer)
    """
    width        = gimp.drawable_width(layer)
    height       = gimp.drawable_height(layer)
    img_type     = gimp.image_base_type(img)
    precision    = gimp.image_get_precision(img)
    sPrecision   = GetInfoImg(precision)[0]
    nimg         = gimp.image_new_with_precision( width, height, img_type, sPrecision )
    nlayer       = gimp.layer_new_from_drawable(layer,nimg)
    gimp.image_insert_layer(nimg,nlayer, None,-1)
    return (nimg, nlayer)

def create_layer(img, layer,layername, mode=None, position=-1, opacity=0.0):
    """ Create a empty layer with the same properties as layer
    """
    width         = gimp.drawable_width(layer)
    height        = gimp.drawable_height(layer)
    drwtype       = gimp.drawable_type(layer)
    if opacity == 0.0 : opacity = gimp.layer_get_opacity(layer)
    if mode == None   : mode = gimp.layer_get_mode(layer)
    new_layer     = gimp.layer_new(img, layername, width, height, drwtype, opacity, mode) 
    # insertion du calque a la position (-1=courante) : hors group
    gimp.image_insert_layer(img,new_layer, None, position)
    gimp.image_set_active_layer( img, new_layer)
    return new_layer

def copy_layer_to_image(src_img, src_layer, dest_img , layername, position=-1 ) :
    """ copy a layer from an image to another image
        if src_img != None then src_img is deleted 
    """
    gimp.image_undo_group_start(dest_img)
    layer_cpy = gimp.layer_new_from_drawable(src_layer,dest_img)
    # insertion du calque a la position (-1=courante) : hors group
    gimp.image_insert_layer(dest_img,layer_cpy, None, position)
    gimp.image_set_active_layer( dest_img, layer_cpy)
    if layername != None : gimp.item_set_name(layer_cpy, layername)
    gimp.image_undo_group_end(dest_img)
    if src_img != None : gimp.image_delete(src_img)
    return layer_cpy

def copy_layer(img, layer , layername, position=-1, alpha=None ) :
    """ copy a layer in the current image
    """
    layer_cpy = gimp.layer_copy(layer,alpha)
    if layername != None : gimp.item_set_name(layer_cpy, layername)    
    if  position != None : 
        # insertion du calque a la position (-1=courante) : hors group
        gimp.image_insert_layer(img,layer_cpy, None, position)
        gimp.image_set_active_layer( img, layer_cpy)
    return layer_cpy

def fill_layer(layer,color):
    gimp.context_set_background( color )
    gimp.drawable_fill(layer, "BACKGROUND_FILL" )

    
def add_mask(img, layer, layer_float, blur=0.0 ):
    mask=gimp.layer_create_mask(layer,"WHITE")
    gimp.layer_add_mask(layer,mask)
    gimp.floating_sel_attach(layer_float,mask)
    gimp.floating_sel_anchor(layer_float)
    if blur > 0.0 : gauss_iir(img, mask, blur, True, True)
    return mask

def add_mask_with_selection(img,layer,blur=0.0):
    mask_layer=gimp.layer_create_mask(layer,"SELECTION")
    gimp.layer_add_mask(layer,mask_layer)
    gimp.selection_none(img)
    if blur > 0.0 : gauss_iir(img, mask_layer, blur, True, True )
    gimp.layer_set_edit_mask(layer,False)
    return mask_layer

def check_compatibilty(proc_name,  img, tdrawable ):
    ''' Check if a layer is not group or vector
        Check if the image precision is compatible with the processing
    '''
    if gimp.item_is_group(tdrawable) or gimp.item_is_vectors(tdrawable):
        msgerr= _("filter") + " " + proc_name + _(" called on a layer-group/vectors item")
        return ( "CALLING_ERROR", msgerr  )
    
    precision = gimp.image_get_precision(img)
    
    if GetInfoImg(precision)[1] == 0 :
        gimp.message( proc_name + " " +_("image : untreated precision =>") + GetInfoImg(precision)[0] + "!!!" )
        return ( "SUCCESS","")
    
    return None

def getMaxDyn(img):
    precision = gimp.image_get_precision(img)
    MaxDyn  = GetInfoImg(precision)[4]
    if GetInfoImg(precision)[1]== 8 : MaxDyn=1.0
    if GetInfoImg(precision)[1]== -32 : # float
        MaxDyn=1.0
    return MaxDyn

def colorize_image(timg,tdrawable, ValueLevel, RedLevel, GreenLevel, BlueLevel, Blur):
    _unused_ = (tdrawable,)
    couleurs = { 'RED':RedLevel, 'GREEN': GreenLevel, 'BLUE': BlueLevel }   
    layer_flatten = gimp.image_flatten(timg)
        
    layer_L = copy_layer(timg, layer_flatten, _("luminance layer"),-1)
    gimp.layer_set_mode(layer_L,"LUMINANCE")
    
    if not gimp.drawable_is_rgb(layer_flatten):  gimp.image_convert_rgb(timg)
       
    if Blur > 0.0 : gauss_iir(timg, layer_flatten, Blur, True, True )
    
    for cle,valeur in couleurs.items() :
        level_out=1.0
        if valeur == 0.0 :
            valeur=1.0
            level_out=0.0
        valeur=max(0.11,valeur)
        gimp.drawable_levels(layer_flatten, cle, 0.0, 1.0, False, valeur, 0.0, level_out , False )
       
    gimp.drawable_levels(layer_L,'VALUE',0.0, 1.0, False, ValueLevel, 0.0,  1.0, False )
    
    return (layer_flatten,layer_L)

# --------------------------------------------------------------------------
def GetPointsVector(timg, nb_point_expected=1, remove_vector=True):
    max_points = 10 ;
    if  nb_point_expected > max_points :
        return None
    
    vectors_active = gimp.image_get_active_vectors(timg)
    if vectors_active <0 :
        return None

    ptr_stroke_ids = gimp.vectors_get_strokes(vectors_active,size_max=max_points)[1]

    stroke_id = (ptr_stroke_ids.contents)[0]
    ( vectors_type, num_points,
       controlpoints, closed ) = gimp.vectors_stroke_get_points(vectors_active,stroke_id )
    _unused = ( vectors_type, closed ) ;

    infos_par_point=6 # {controle1} {centre} {controle2}
    if num_points < nb_point_expected*infos_par_point :
        return None

    xy_pts=[]
    for ii in range(nb_point_expected):
        xy_pts.append( (controlpoints[2+ii*infos_par_point],controlpoints[3+ii*infos_par_point]) )

    if remove_vector :
        gimp.image_remove_vectors(timg,vectors_active)

    return xy_pts

    