#! /usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
import os

# ------------------------------------------------------------------------------
# import local lib
path_exec=os.path.dirname(os.path.dirname( __file__ ))
if not path_exec :
    path_exec='..'
sys.path.append(path_exec)
sys.path.append(os.path.join(path_exec,"gimp2"))

import gp2_func  as gimp
from plug_in    import decompose,compose
from libTools   import copy_layer_to_image,create_img
# ----------------------------------------------------------------------
def GetLuminanceFromRGBImage(timg, layerRGB ):
    """ extracts the luminance of an RGB image """
    (img_Ls, img_A, img_B, unused) = decompose(timg,layerRGB,"LAB", False)[1:]
    gimp.image_delete(img_A)
    gimp.image_delete(img_B)
    (layer_Ls,) = gimp.get_layers(img_Ls)
    return copy_layer_to_image(img_Ls, layer_Ls, timg , "RGB->Ls",-1 )

# ----------------------------------------------------------------------
def ReplaceColorFromRGBImage(timg, images_RGB, newlayer_color,  colorletter, layername ):
    (img_R, img_G, img_B)  = images_RGB
    if colorletter.upper() =="G" :
        layer = gimp.layer_new_from_drawable(newlayer_color, img_G)
        gimp.image_insert_layer(img_G,layer, None,-1)
        gimp.image_flatten(img_G)
    elif colorletter.upper() =="B" :
        layer = gimp.layer_new_from_drawable(newlayer_color, img_B)
        gimp.image_insert_layer(img_B,layer, None,-1)
        gimp.image_flatten(img_B)
    else :
        layer = gimp.layer_new_from_drawable(newlayer_color, img_R)
        gimp.image_insert_layer(img_R,layer, None,-1)
        gimp.image_flatten(img_R)
        colorletter="R"
        
    img_RGB  = compose(img_R, None, img_G, img_B, None, "RGB")[1]
    gimp.image_delete(img_R)
    gimp.image_delete(img_G)
    gimp.image_delete(img_B)
    
    newlayer_RGB = gimp.get_layers(img_RGB)[0]
    newlayer_RGB_cpy = gimp.layer_new_from_drawable(newlayer_RGB,timg)
    gimp.image_insert_layer(timg,newlayer_RGB_cpy, None,1)
    gimp.image_delete(img_RGB)
    
    gimp.image_set_active_layer( timg, newlayer_RGB_cpy)
    gimp.item_set_name(newlayer_RGB_cpy,layername)
    return gimp.get_layers(timg)[0]

# ----------------------------------------------------------------------
def GetColorFromRGBImage(timg, layerRGB, colorletter="R" ):
    """ Extracts a color from RGB image """
    (img_R, img_G, img_B, unused)  = decompose(timg,layerRGB,"RGB", False)[1:]
    if colorletter.upper() =="G" :
        layer_color = gimp.get_layers(img_G)[0]
    elif colorletter.upper() =="B" :
        layer_color = gimp.get_layers(img_B)[0]
    else :
        layer_color = gimp.get_layers(img_R)[0]
        colorletter="R"

    layer = copy_layer_to_image(None, layer_color, timg, "RGB->"+colorletter.upper(), -1 )

    return (layer, (img_R, img_G, img_B ))
    
# ----------------------------------------------------------------------
def GetRedFromRGBImage(timg, layerRGB ):
    return GetColorFromRGBImage(timg, layerRGB,'R')
    
# ----------------------------------------------------------------------
def GetGreenFromRGBImage(timg, layerRGB ):
    return GetColorFromRGBImage(timg, layerRGB,'G')
    
# ----------------------------------------------------------------------
def GetBlueFromRGBImage(self, timg, layerRGB ):
    return GetColorFromRGBImage(timg, layerRGB,'B')
    
# ----------------------------------------------------------------------
def GetMix2layers(timg, layer1,layer2, mix_pourcent, layername ):
    """ mix 2 layers : layer1 + x pourcent of layer2 """   
    nimg = create_img(timg, layer1)[0]    
    nlayer2=gimp.layer_new_from_drawable(layer2,nimg)
    gimp.image_insert_layer(nimg,nlayer2, None,-1)    
    gimp.layer_set_opacity(nlayer2,mix_pourcent)
    layer = gimp.image_flatten(nimg)   
    return copy_layer_to_image(nimg, layer, timg, layername, -1 )
    