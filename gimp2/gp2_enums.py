#! /usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------

GimpRunMode={
    "INTERACTIVE"   : 0,
    "NONINTERACTIVE": 1
    }
GimpPDBProcType={
    "GIMP_INTERNAL"   : 0, # Internal GIMP procedure
    "GIMP_PLUGIN"     : 1, # GIMP Plug-In
    "GIMP_EXTENSION"  : 2, # GIMP Extension
    "GIMP_TEMPORARY"  : 3  # Temporary Procedure
    }

GimpPDBStatusType={
    "EXECUTION_ERROR" : 0,
    "CALLING_ERROR"   : 1,
    "PASS_THROUGH"    : 2,
    "SUCCESS"         : 3,
    "CANCEL"          : 4
    }

GimpPDBArgType= {
            "INT32"      : 0,
            "INT16"      : 1,
            "INT8"       : 2,
            "FLOAT"      : 3,
            "STRING"     : 4,
            "INT32ARRAY" : 5,
            "INT16ARRAY" : 6,
            "INT8ARRAY"  : 7,
            "FLOATARRAY" : 8,
            "STRINGARRAY": 9,
            "COLOR"      : 10,
            "ITEM"       : 11,
            "DISPLAY"    : 12,
            "IMAGE"      : 13,
            "LAYER"      : 14,
            "CHANNEL"    : 15,
            "DRAWABLE"   : 16,
            "SELECTION"  : 17,
            "COLORARRAY" : 18,
            "VECTORS"    : 19,
            "PARASITE"   : 20,
            "STATUS"     : 21,
            "END"        : 22
            }

GimpLayerMode={
        "NORMAL_LEGACY"            : 0  ,
        "DISSOLVE"                 : 1  ,
        "BEHIND_LEGACY"            : 2  ,
        "MULTIPLY_LEGACY"          : 3  ,
        "SCREEN_LEGACY"            : 4  ,
        "OVERLAY_LEGACY"           : 5  ,
        "DIFFERENCE_LEGACY"        : 6  ,
        "ADDITION_LEGACY"          : 7  ,
        "SUBTRACT_LEGACY"          : 8  ,
        "DARKEN_ONLY_LEGACY"       : 9  ,
        "LIGHTEN_ONLY_LEGACY"      : 10 ,
        "HSV_HUE_LEGACY"           : 11 ,
        "HSV_SATURATION_LEGACY"    : 12 ,
        "HSL_COLOR_LEGACY"         : 13 ,
        "HSV_VALUE_LEGACY"         : 14 ,
        "DIVIDE_LEGACY"            : 15 ,
        "DODGE_LEGACY"             : 16 ,
        "BURN_LEGACY"              : 17 ,
        "HARDLIGHT_LEGACY"         : 18 ,
        "SOFTLIGHT_LEGACY"         : 19 ,
        "GRAIN_EXTRACT_LEGACY"     : 20 ,
        "GRAIN_MERGE_LEGACY"       : 21 ,
        "COLOR_ERASE_LEGACY"       : 22 ,
        "OVERLAY"                  : 23 ,
        "LCH_HUE"                  : 24 ,
        "LCH_CHROMA"               : 25 ,
        "LCH_COLOR"                : 26 ,
        "LCH_LIGHTNESS"            : 27 ,
        "NORMAL"                   : 28 ,
        "BEHIND"                   : 29 ,
        "MULTIPLY"                 : 30 ,
        "SCREEN"                   : 31 ,
        "DIFFERENCE"               : 32 ,
        "ADDITION"                 : 33 ,
        "SUBTRACT"                 : 34 ,
        "DARKEN_ONLY"              : 35 ,
        "LIGHTEN_ONLY"             : 36 ,
        "HSV_HUE"                  : 37 ,
        "HSV_SATURATION"           : 38 ,
        "HSL_COLOR"                : 39 ,
        "HSV_VALUE"                : 40 ,
        "DIVIDE"                   : 41 ,
        "DODGE"                    : 42 ,
        "BURN"                     : 43 ,
        "HARDLIGHT"                : 44 ,
        "SOFTLIGHT"                : 45 ,
        "GRAIN_EXTRACT"            : 46 ,
        "GRAIN_MERGE"              : 47 ,
        "VIVID_LIGHT"              : 48 ,
        "PIN_LIGHT"                : 49 ,
        "LINEAR_LIGHT"             : 50 ,
        "HARD_MIX"                 : 51 ,
        "EXCLUSION"                : 52 ,
        "LINEAR_BURN"              : 53 ,
        "LUMA_DARKEN_ONLY"         : 54 ,
        "LUMA_LIGHTEN_ONLY"        : 55 ,
        "LUMINANCE"                : 56 ,
        "COLOR_ERASE"              : 57 ,
        "ERASE"                    : 58 ,
        "MERGE"                    : 59 ,
        "SPLIT"                    : 60 ,
        "PASS_THROUGH"             : 61
        }
# ancien mode
GimpLayerModeEffects = {
        "NORMAL"       :  0,
        "DISSOLVE"     :  1,
        "BEHIND"       :  2,
        "MULTIPLY"     :  3,
        "SCREEN"       :  4,
        "OVERLAY"      :  5,
        "DIFFERENCE"   :  6,
        "ADDITION"     :  7,
        "SUBTRACT"     :  8,
        "DARKEN_ONLY"  :  9,
        "LIGHTEN_ONLY" : 10,
        "HUE"          : 11,
        "SATURATION"   : 12,
        "COLOR"        : 13,
        "VALUE"        : 14,
        "DIVIDE"       : 15,
        "DODGE"        : 16,
        "BURN"         : 17,
        "HARDLIGHT"    : 18,
        "SOFTLIGHT"    : 19,
        "GRAIN_EXTRACT": 20,
        "GRAIN_MERGE"  : 21,
        "COLOR_ERASE"  : 22
    }

GimpHistogramChannel =  {
    "VALUE"    : 0,
    "RED"      : 1,
    "GREEN"    : 2,
    "BLUE"     : 3,
    "ALPHA"    : 4,
    "LUMINANCE": 5
    }
GimpMergeType={
        "EXPAND_AS_NECESSARY" : 0 ,
        "CLIP_TO_IMAGE"       : 1 ,
        "CLIP_TO_BOTTOM_LAYER": 2 ,
        "FLATTEN_IMAGE"       : 3
    }
GimpSizeType = {
        "PIXELS" : 0,
        "POINTS" :1
    }

GimpPrecision={
    "U8_LINEAR"         : 100, # 8-bit linear integer
    "U8_NON_LINEAR"     : 150, # 8-bit non-linear integer
    "U8_PERCEPTUAL"     : 175, # 8-bit perceptual integer
    "U16_LINEAR"        : 200, # 16-bit linear integer
    "U16_NON_LINEAR"    : 250, # 16-bit non-linear integer
    "U16_PERCEPTUAL"    : 275, # 16-bit perceptual integer
    "U32_LINEAR"        : 300, # 32-bit linear integer
    "U32_NON_LINEAR"    : 350, # 32-bit non-linear integer
    "U32_PERCEPTUAL"    : 375, # 32-bit perceptual integer
    "HALF_LINEAR"       : 500, # 16-bit linear floating point
    "HALF_NON_LINEAR"   : 550, # 16-bit non-linear floating point
    "HALF_PERCEPTUAL"   : 575, # 16-bit perceptual floating point
    "FLOAT_LINEAR"      : 600, # 32-bit linear floating point
    "FLOAT_NON_LINEAR"  : 650, # 32-bit non-linear floating point
    "FLOAT_PERCEPTUAL"  : 675, # 32-bit perceptual floating point
    "DOUBLE_LINEAR"     : 700, # 64-bit linear floating point
    "DOUBLE_NON_LINEAR" : 750, # 64-bit non-linear floating point
    "DOUBLE_PERCEPTUAL" : 775  # 64-bit perceptual floating point
    ## #ifndef GIMP_DISABLE_DEPRECATED
    ## GIMP_PRECISION_U8_GAMMA      = GIMP_PRECISION_U8_NON_LINEAR,
    ## GIMP_PRECISION_U16_GAMMA     = GIMP_PRECISION_U16_NON_LINEAR,
    ## GIMP_PRECISION_U32_GAMMA     = GIMP_PRECISION_U32_NON_LINEAR,
    ## GIMP_PRECISION_HALF_GAMMA    = GIMP_PRECISION_HALF_NON_LINEAR,
    ## GIMP_PRECISION_FLOAT_GAMMA   = GIMP_PRECISION_FLOAT_NON_LINEAR,
    ## GIMP_PRECISION_DOUBLE_GAMMA  = GIMP_PRECISION_DOUBLE_NON_LINEAR
    ## #endif
    }

GimpChannelOps={
        "ADD"        : 0,  # Add to the current selection
        "SUBTRACT"   : 1,  # Subtract from the current selection
        "REPLACE"    : 2,  # Replace the current selection
        "INTERSECT"  : 3   # Intersect with the current selection
    }


GimpImageType={
        "RGB_IMAGE"     : 0,
        "RGBA_IMAGE"    : 1,
        "GRAY_IMAGE"    : 2,
        "GRAYA_IMAGE"   : 3,
        "INDEXED_IMAGE" : 4,
        "INDEXEDA_IMAGE": 5
}

GimpImageBaseType = {
        "RGB"     : 0 , # "RGB color"
        "GRAY"    : 1 , # "Grayscale"
        "INDEXED" : 2   # "Indexed color"
}

GimpFillType={
        "FOREGROUND_FILL" : 0,
        "BACKGROUND_FILL" : 1,
        "WHITE_FILL"      : 2,
        "TRANSPARENT_FILL": 3,
        "PATTERN_FILL"    : 4
    }

GimpBrushGeneratedShape={
        "CIRCLE"  : 0,
        "SQUARE"  : 1,
        "DIAMOND" : 2
    }

GimpVectorsStrokeType={
    "VECTORS_STROKE_TYPE_BEZIER":0
}

GimpAddMaskType={
        "WHITE"         : 0, # White (full opacity)
        "BLACK"         : 1, # Black (full transparency)
        "ALPHA"         : 2, # Layer's _alpha channel
        "ALPHA_TRANSFER": 3, # Transfer layer's alpha channel
        "SELECTION"     : 4, # Selection
        "COPY"          : 5, # Grayscale copy of layer
        "CHANNEL"       : 6  # C_hannel
    }
GimpDesaturateMode = {
  "LIGHTNESS"  : 0, # Lightness
  "LUMINOSITY" : 1, # Luminosity
  "AVERAGE"    : 2,  # Average
  "LUMINANCE"  : 3
} 

GimpHueRange = {
    "ALL_HUES"    : 0,
    "RED_HUES"    : 1,
    "YELLOW_HUES" : 2,
    "GREEN_HUES"  : 3,
    "CYAN_HUES"   : 4,
    "BLUE_HUES"   : 5,
    "MAGENTA_HUES": 6
} 
