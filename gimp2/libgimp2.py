#! /usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import os
import traceback
from ctypes import CDLL
from sys    import platform

__ALL__ = [ 'gimp2', 'gegl'  ]

def load_library (library_name):
    from ctypes.util import find_library
    library_name_exact=None
    if platform == "darwin" :
        for path in ( '/usr/local/lib/', '/usr/lib/', '/lib/'):
            library_name_test = path + library_name + '.0.dylib'
            print( library_name_test + " exist ?" )
            if os.path.exists(library_name_test) == True :
                library_name_exact = library_name_test
                break
            print( "-> no" )
    elif platform == "linux" or platform == "linux2":
        library_name_exact = find_library(library_name[3:] )
    elif platform == "win32":
        library_name_exact = find_library(library_name + "-0")
    else:
        raise BaseException ("TODO")
    if library_name_exact is not None : 
        print("loading " + library_name_exact )
    else:
        print("Error: not found " + library_name )

    return CDLL(library_name_exact)
try:
    gimp2 = load_library ('libgimp-2.0')
    gegl  = load_library ('libgegl-0.4')
    gegl.gegl_init (None, None)
except :
    print(traceback.format_exc())
    gimp2 = None
    gegl = None
