#! /usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
from array  import array

import gp2_func as gimp
from gp2_enums import GimpPrecision

InfoPrecision = {
    GimpPrecision["U8_LINEAR"         ]: ["U8_LINEAR"         ,  8, 'B' , 0, (2**8)-1 ],
    GimpPrecision["U8_NON_LINEAR"     ]: ["U8_NON_LINEAR"     ,  8, 'B' , 0, (2**8)-1 ],
    GimpPrecision["U8_PERCEPTUAL"     ]: ["U8_PERCEPTUAL"     ,  8, 'B' , 0, (2**8)-1 ],
    GimpPrecision["U16_LINEAR"        ]: ["U16_LINEAR"        , 16, 'H' , 0, (2**16)-1],
    GimpPrecision["U16_NON_LINEAR"    ]: ["U16_NON_LINEAR"    , 16, 'H' , 0, (2**16)-1],
    GimpPrecision["U16_PERCEPTUAL"    ]: ["U16_PERCEPTUAL"    , 16, 'H' , 0, (2**16)-1],
    GimpPrecision["U32_LINEAR"        ]: ["U32_LINEAR"        , 32, 'L' , 0, (2**32)-1],
    GimpPrecision["U32_NON_LINEAR"    ]: ["U32_NON_LINEAR"    , 32, 'L' , 0, (2**32)-1],
    GimpPrecision["U32_PERCEPTUAL"    ]: ["U32_PERCEPTUAL"    , 32, 'L' , 0, (2**32)-1],
    GimpPrecision["HALF_LINEAR"       ]: ["HALF_LINEAR"       ,  0, '-' , 0, 0      ],
    GimpPrecision["HALF_NON_LINEAR"   ]: ["HALF_NON_LINEAR"   ,  0, '-' , 0, 0      ],
    GimpPrecision["HALF_PERCEPTUAL"   ]: ["HALF_PERCEPTUAL"   ,  0, '-' , 0, 0      ],
    GimpPrecision["FLOAT_LINEAR"      ]: ["FLOAT_LINEAR"      ,-32, 'f' , 0, 1.0    ],
    GimpPrecision["FLOAT_NON_LINEAR"  ]: ["FLOAT_NON_LINEAR"  ,-32, 'f' , 0, 1.0    ],
    GimpPrecision["FLOAT_PERCEPTUAL"  ]: ["FLOAT_PERCEPTUAL"  ,-32, 'f' , 0, 1.0    ],
    GimpPrecision["DOUBLE_LINEAR"     ]: ["DOUBLE_LINEAR"     ,  0, '-' , 0, 0      ],
    GimpPrecision["DOUBLE_NON_LINEAR" ]: ["DOUBLE_NON_LINEAR" ,  0, '-' , 0, 0      ],
    GimpPrecision["DOUBLE_PERCEPTUAL" ]: ["DOUBLE_PERCEPTUAL" ,  0, '-' , 0, 0      ]
}
def GetInfoImg( precision ):
    return InfoPrecision[precision]


def GetTextBoundingBox( text, size, font, size_type="PIXELS" ):
    return gimp.text_get_extents_fontname( text, size, size_type, font )[1]

def SetText(timg, x,y,text,fontname, szfont, color, justify,  size_type="PIXELS" ):
    (tw,th,th0,th1) = GetTextBoundingBox( text, szfont, fontname, size_type )
    gimp.context_set_foreground( color )
    xx=x
    yy=y
    if justify==1 :
        xx = x - tw
    if justify==2 :
        xx = x - tw/2

    tlayer=gimp.text_fontname( timg, -1 , xx, yy, text, -1, True, szfont, size_type, fontname)

    return (tlayer,(xx,yy,tw,th,th0,th1))

def GetRegionRect(tdrawable, fmt, x0, y0, width, height ):
    sPixelRgn = gimp.pixel_rgn_init( tdrawable, x0, y0, width, height, False, False )
    srcImgRgn = gimp.pixel_rgn_get_rect(sPixelRgn, x0, y0,width, height)

    srcImgPix = array(fmt)
    try:
        srcImgPix.frombytes(srcImgRgn)
    except: # python 2.7
        srcImgPix.fromstring(srcImgRgn)

    return (sPixelRgn, srcImgRgn, srcImgPix )

def UpdateRegionRect(tdrawable, sPixelRgn, srcImgRgn,srcImgPix ):
    width  = gimp.drawable_width (tdrawable)
    height = gimp.drawable_height(tdrawable)
    try:
        srcImgRgn[:] = srcImgPix.tobytes()
    except: # python 2.7
        srcImgRgn[:] = [ ord(b) for b in srcImgPix.tostring() ]
    gimp.pixel_rgn_set_rect(sPixelRgn,srcImgRgn,0,0,width, height)
    #gimp.drawable_flush()
    #gimp.drawable_merge_shadow(tdrawable,True)
    gimp.drawable_update(tdrawable, 0, 0, width, height)

def SelectionBounds( id_img ):
    (cr,selection) = gimp.selection_bounds(id_img)
    if not cr :
        selection = ( False, 0, 0, 0, 0 )
    return selection
