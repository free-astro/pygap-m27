#! /usr/bin/env python
# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
import os
import re

Pkg="GapM27"
Soft="py"+Pkg

# Redirect the print statement to file (usefull for debugging)
__DEBUG = True
if __DEBUG:
    if os.name == 'nt':
        myhome=os.environ.get('USERPROFILE')
    else:
        myhome=os.environ.get('HOME')
    sys.stdout.close()
    sys.stdout = open( os.path.join( myhome , Soft+'_stdout.txt'), 'a+')
    sys.stderr = sys.stdout

import gettext
import locale

# ==============================================================================
def stdoutflush():
    sys.stdout.flush()

def stdout_clear():
    if __DEBUG:
        sys.stdout.close()
        sys.stdout = open( os.path.join( myhome , Soft+'_stdout.txt'), 'w')
        sys.stderr = sys.stdout
       
# ==============================================================================
def resource_path(relative_path, pathabs=None):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path =  getattr(sys, '_MEIPASS')
    except AttributeError : # pylint: disable=bare-except
        if pathabs== None :
            base_path = os.path.dirname( sys.argv[0] )
        else:
            base_path = pathabs

    return os.path.join(base_path, relative_path)


# ==============================================================================
def init_language( langue_selected =None ):
    try:
        if langue_selected == None :
            if sys.platform.startswith('win'):
                if os.getenv('LANG') is None:
                    lang, enc = locale.getdefaultlocale()
                    os.environ['LANG'] = lang
                    if len(enc) >0:
                        os.environ['LANG'] += "." + enc
            langue_selected = os.environ['LANG']

        langue= langue_selected.split('.' )[0]
        if not re.search( "_" ,langue ) :
            langue = langue.lower() + "_" + langue.upper()

        path_abs=os.path.dirname(os.path.abspath(__file__))
        i18n_dir = resource_path( os.path.join('i18n' ,langue),pathabs=path_abs )

        if not os.path.exists(i18n_dir )  :
            gettext.install(Soft)
        else:
            lang = gettext.translation(Soft, os.path.dirname(i18n_dir) , languages=[langue,langue_selected] )
            lang.install()
    except Exception as e :
        print("*** error ***", str(e))
        gettext.install(Soft)


# ==============================================================================
# Constantes
init_language()

menu_path       = "<Image>/" + Soft
menu_sun        = menu_path+"/"+_("Sun")
menu_deepsky    = menu_path+"/"+_("Deep Sky")
menu_color      = menu_path+"/"+_("Color")
menu_composit   = menu_deepsky+"/"+_("Compositing")
menu_stars      = menu_deepsky+"/"+_("Stars")
menu_noise      = menu_deepsky+"/"+_("Noise reduction")
menu_enhancement= menu_deepsky+"/"+_("Enhancement")
menu_background = menu_deepsky+"/"+_("Sky background")
menu_tools      = menu_path+"/"+_("Tools")
menu_layertools = menu_tools+"/"+_("Layers")
menu_imagetools = menu_tools+"/"+_("Images")
menu_cartridge  = menu_path+"/"+_("DrawingCartridge")
menu_filters    = menu_path+"/"+_("Filters")
menu_planet     = menu_path+"/"+_("Planets")
menu_moon       = menu_path+"/"+_("Moon")

version_plugin="0.0.20"
date_plugin   ="2019-2024"
license_plugin="LGPL-v3"

info_right0  = {
    "author"    : "M27trognondepomme" ,
    "copyright" : "M27trognondepomme" ,
    "version"   : version_plugin  ,
    "date"      : date_plugin ,
    "license"   : license_plugin
    }

info_right1  = {
    "author"    : "Bill Smith/M27trognondepomme" ,
    "copyright" : "Bill Smith/M27trognondepomme" ,
    "version"   : version_plugin  ,
    "date"      : "2016/"+date_plugin ,
    "license"   : license_plugin
    }

bullet      = "  o "
tab         = "   "
cr          = "\n"
